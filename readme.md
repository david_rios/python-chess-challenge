# Python Chess Challenge

## Introduction

The problem description can be found [here](docs/problem.md).


## Instructions to run it

Testing it:

    python3 chess_problem_tests.py

Running it:

    python3 chess_problem.py 7x7 K:2 Q:2 B:2 N:1

To see all options:

    python3 chess_problem.py --help