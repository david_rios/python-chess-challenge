import sys
from setuptools import setup

if sys.version_info[:2] < (3, 4):
    raise Exception('This package is Python 3.4+ only.')

setup(
    name='chess_problem',
    version='0.0.1',
    description='',
    author='David Rios',
    author_email='david.rios.gomes@gmail.com',
    py_modules=['chess_problem'],
    install_requires=[],
    zip_safe=False,
    entry_points={
        'console_scripts': ['chess-problem = chess_problem:cli_main']
    }
)
