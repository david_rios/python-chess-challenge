import itertools
import logging
import sys
import unittest
from collections import Counter
from random import randint, choice

from chess_problem import *

logging.basicConfig(level=logging.ERROR)


class ZChessProblemCase(unittest.TestCase):

    def test_example1(self):
        solver = ChessProblemSolver(BoardSize(3, 3), {'K': 2, 'R': 1})
        result = [set(i) for i in solver.solve()]
        expected = [
            {('K', Point(0, 0)), ('K', Point(2, 0)), ('R', Point(1, 2))},
            {('K', Point(0, 0)), ('K', Point(0, 2)), ('R', Point(2, 1))},
            {('K', Point(2, 0)), ('K', Point(2, 2)), ('R', Point(0, 1))},
            {('K', Point(0, 2)), ('K', Point(2, 2)), ('R', Point(1, 0))},
        ]

        self.assertEqual(len(result), len(expected))

        for item in expected:
            with self.subTest(item=item):
                self.assertIn(item, result)

    def test_example2(self):
        solver = ChessProblemSolver(BoardSize(4, 4), {'R': 2, 'N': 4})
        result = [set(i) for i in solver.solve()]
        expected = [
            {('R', Point(x=0, y=0)), ('R', Point(x=2, y=2)), ('N', Point(x=1, y=1)), ('N', Point(x=3, y=1)), ('N', Point(x=1, y=3)), ('N', Point(x=3, y=3))},
            {('R', Point(x=1, y=0)), ('R', Point(x=3, y=2)), ('N', Point(x=0, y=1)), ('N', Point(x=2, y=1)), ('N', Point(x=0, y=3)), ('N', Point(x=2, y=3))},
            {('R', Point(x=2, y=0)), ('R', Point(x=0, y=2)), ('N', Point(x=1, y=1)), ('N', Point(x=3, y=1)), ('N', Point(x=1, y=3)), ('N', Point(x=3, y=3))},
            {('R', Point(x=3, y=0)), ('R', Point(x=1, y=2)), ('N', Point(x=0, y=1)), ('N', Point(x=2, y=1)), ('N', Point(x=0, y=3)), ('N', Point(x=2, y=3))},
            {('R', Point(x=0, y=1)), ('R', Point(x=2, y=3)), ('N', Point(x=1, y=0)), ('N', Point(x=3, y=0)), ('N', Point(x=1, y=2)), ('N', Point(x=3, y=2))},
            {('R', Point(x=1, y=1)), ('R', Point(x=3, y=3)), ('N', Point(x=0, y=0)), ('N', Point(x=2, y=0)), ('N', Point(x=0, y=2)), ('N', Point(x=2, y=2))},
            {('R', Point(x=2, y=1)), ('R', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('N', Point(x=3, y=0)), ('N', Point(x=1, y=2)), ('N', Point(x=3, y=2))},
            {('R', Point(x=3, y=1)), ('R', Point(x=1, y=3)), ('N', Point(x=0, y=0)), ('N', Point(x=2, y=0)), ('N', Point(x=0, y=2)), ('N', Point(x=2, y=2))},
        ]

        self.assertEqual(len(result), len(expected))

        for item in expected:
            with self.subTest(item=item):
                self.assertIn(item, result)

    def test_5x5_k3_r3(self):
        solver = ChessProblemSolver(BoardSize(5, 5), {'K': 3, 'R': 3})
        result = [set(i) for i in solver.solve()]
        expected = [
            {('K', Point(x=0, y=4)), ('R', Point(x=2, y=3)), ('R', Point(x=3, y=1)), ('K', Point(x=4, y=4)), ('K', Point(x=0, y=2)), ('R', Point(x=1, y=0))},
            {('K', Point(x=0, y=4)), ('R', Point(x=4, y=3)), ('R', Point(x=3, y=1)), ('K', Point(x=0, y=2)), ('K', Point(x=2, y=4)), ('R', Point(x=1, y=0))},
            {('K', Point(x=4, y=2)), ('R', Point(x=3, y=0)), ('K', Point(x=4, y=4)), ('K', Point(x=2, y=4)), ('R', Point(x=1, y=1)), ('R', Point(x=0, y=3))},
            {('K', Point(x=0, y=4)), ('K', Point(x=4, y=2)), ('R', Point(x=2, y=3)), ('R', Point(x=3, y=0)), ('K', Point(x=4, y=4)), ('R', Point(x=1, y=1))},
            {('R', Point(x=3, y=2)), ('R', Point(x=1, y=3)), ('R', Point(x=0, y=1)), ('K', Point(x=4, y=0)), ('K', Point(x=4, y=4)), ('K', Point(x=2, y=0))},
            {('R', Point(x=1, y=3)), ('K', Point(x=4, y=2)), ('R', Point(x=0, y=1)), ('K', Point(x=4, y=0)), ('R', Point(x=3, y=4)), ('K', Point(x=2, y=0))},
            {('R', Point(x=3, y=2)), ('K', Point(x=4, y=0)), ('K', Point(x=4, y=4)), ('K', Point(x=2, y=4)), ('R', Point(x=1, y=1)), ('R', Point(x=0, y=3))},
            {('R', Point(x=3, y=2)), ('K', Point(x=0, y=4)), ('R', Point(x=2, y=3)), ('K', Point(x=4, y=0)), ('K', Point(x=4, y=4)), ('R', Point(x=1, y=1))},
            {('K', Point(x=0, y=4)), ('R', Point(x=1, y=2)), ('K', Point(x=0, y=0)), ('K', Point(x=4, y=0)), ('R', Point(x=2, y=1)), ('R', Point(x=3, y=3))},
            {('R', Point(x=3, y=2)), ('R', Point(x=1, y=3)), ('K', Point(x=0, y=0)), ('K', Point(x=4, y=0)), ('K', Point(x=4, y=4)), ('R', Point(x=2, y=1))},
            {('R', Point(x=1, y=3)), ('K', Point(x=4, y=2)), ('K', Point(x=0, y=0)), ('K', Point(x=4, y=0)), ('R', Point(x=3, y=4)), ('R', Point(x=2, y=1))},
            {('R', Point(x=3, y=3)), ('K', Point(x=0, y=0)), ('K', Point(x=4, y=0)), ('K', Point(x=0, y=2)), ('R', Point(x=1, y=4)), ('R', Point(x=2, y=1))},
            {('K', Point(x=0, y=4)), ('R', Point(x=1, y=2)), ('K', Point(x=0, y=0)), ('R', Point(x=2, y=3)), ('R', Point(x=3, y=1)), ('K', Point(x=4, y=4))},
            {('K', Point(x=0, y=4)), ('R', Point(x=4, y=3)), ('R', Point(x=1, y=2)), ('K', Point(x=0, y=0)), ('R', Point(x=3, y=1)), ('K', Point(x=2, y=4))},
            {('R', Point(x=3, y=3)), ('K', Point(x=0, y=4)), ('R', Point(x=1, y=2)), ('K', Point(x=0, y=0)), ('R', Point(x=4, y=1)), ('K', Point(x=2, y=0))},
            {('R', Point(x=3, y=3)), ('K', Point(x=0, y=0)), ('K', Point(x=0, y=2)), ('R', Point(x=4, y=1)), ('R', Point(x=1, y=4)), ('K', Point(x=2, y=0))},
        ]

        self.assertEqual(len(result), len(expected))

        for item in expected:
            with self.subTest(item=item):
                self.assertIn(item, result)

    def test_5x5_k1_q2_b1_n1(self):
        solver = ChessProblemSolver(BoardSize(5, 5), {'K': 1, 'Q': 2, 'B': 1, 'N': 1})
        result = [set(i) for i in solver.solve()]
        self.skipTest('lol')
        expected = [
            {('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=2, y=1)), ('Q', Point(x=0, y=0)), ('K', Point(x=4, y=2))},
            {('N', Point(x=1, y=4)), ('B', Point(x=3, y=4)), ('Q', Point(x=2, y=1)), ('Q', Point(x=0, y=0)), ('K', Point(x=4, y=2))},
            {('N', Point(x=1, y=4)), ('B', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('Q', Point(x=0, y=0)), ('K', Point(x=3, y=4))},
            {('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=2, y=1)), ('Q', Point(x=0, y=0)), ('K', Point(x=3, y=4))},
            {('B', Point(x=4, y=3)), ('K', Point(x=1, y=2)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=0)), ('Q', Point(x=3, y=1))},
            {('B', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=0)), ('Q', Point(x=3, y=1))},
            {('B', Point(x=1, y=2)), ('K', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=0)), ('Q', Point(x=3, y=1))},
            {('B', Point(x=2, y=4)), ('K', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=0)), ('Q', Point(x=3, y=1))},
            {('B', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4)), ('Q', Point(x=0, y=0))},
            {('B', Point(x=3, y=1)), ('K', Point(x=4, y=3)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('Q', Point(x=1, y=2))},
            {('B', Point(x=2, y=4)), ('K', Point(x=4, y=3)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('Q', Point(x=1, y=2))},
            {('B', Point(x=3, y=1)), ('K', Point(x=2, y=4)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('Q', Point(x=1, y=2))},
            {('K', Point(x=2, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=3)), ('Q', Point(x=0, y=0)), ('Q', Point(x=1, y=2))},
            {('K', Point(x=2, y=1)), ('Q', Point(x=4, y=2)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=0)), ('B', Point(x=1, y=3))},
            {('K', Point(x=2, y=1)), ('Q', Point(x=4, y=2)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=0)), ('B', Point(x=3, y=4))},
            {('B', Point(x=2, y=1)), ('Q', Point(x=4, y=2)), ('K', Point(x=3, y=4)), ('Q', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=1, y=3)), ('Q', Point(x=4, y=2)), ('K', Point(x=3, y=4)), ('Q', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('K', Point(x=2, y=1)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=2)), ('Q', Point(x=0, y=0)), ('Q', Point(x=1, y=3))},
            {('K', Point(x=2, y=1)), ('N', Point(x=4, y=1)), ('B', Point(x=3, y=4)), ('Q', Point(x=0, y=0)), ('Q', Point(x=1, y=3))},
            {('B', Point(x=2, y=1)), ('N', Point(x=4, y=1)), ('K', Point(x=3, y=4)), ('Q', Point(x=0, y=0)), ('Q', Point(x=1, y=3))},
            {('N', Point(x=4, y=1)), ('B', Point(x=4, y=2)), ('K', Point(x=3, y=4)), ('Q', Point(x=0, y=0)), ('Q', Point(x=1, y=3))},
            {('B', Point(x=1, y=2)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=2, y=4)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=3, y=1)), ('K', Point(x=1, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('K', Point(x=2, y=1)), ('B', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=0)), ('N', Point(x=4, y=3))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=3)), ('Q', Point(x=0, y=0))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=4, y=3)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=4, y=3)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('B', Point(x=1, y=2))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=2, y=1)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('B', Point(x=4, y=2))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=2, y=1)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('B', Point(x=1, y=3))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=1, y=3)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('B', Point(x=2, y=1))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=1, y=3)), ('N', Point(x=4, y=1)), ('Q', Point(x=0, y=0)), ('B', Point(x=4, y=2))},
            {('N', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('B', Point(x=2, y=3)), ('Q', Point(x=3, y=1))},
            {('N', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=3, y=1)), ('B', Point(x=4, y=4))},
            {('N', Point(x=4, y=4)), ('Q', Point(x=1, y=0)), ('B', Point(x=2, y=4)), ('Q', Point(x=3, y=1)), ('K', Point(x=0, y=3))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('K', Point(x=2, y=3)), ('Q', Point(x=3, y=1))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('K', Point(x=2, y=3)), ('Q', Point(x=3, y=1)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('K', Point(x=2, y=4)), ('B', Point(x=0, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=4, y=4))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('K', Point(x=4, y=4)), ('Q', Point(x=3, y=1))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('B', Point(x=2, y=3)), ('K', Point(x=4, y=4)), ('Q', Point(x=3, y=1))},
            {('N', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('B', Point(x=2, y=3)), ('K', Point(x=4, y=4)), ('Q', Point(x=3, y=1))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=4))},
            {('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=4)), ('B', Point(x=3, y=4))},
            {('N', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('B', Point(x=2, y=2))},
            {('N', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('B', Point(x=0, y=4))},
            {('N', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('B', Point(x=3, y=4))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4))},
            {('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4)), ('B', Point(x=0, y=4))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('B', Point(x=0, y=2))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('B', Point(x=0, y=3))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('B', Point(x=2, y=4))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('B', Point(x=3, y=4))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=2, y=4)), ('Q', Point(x=4, y=1)), ('B', Point(x=3, y=4))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4)), ('K', Point(x=0, y=3))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4)), ('K', Point(x=0, y=3))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=3))},
            {('Q', Point(x=1, y=0)), ('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=3)), ('B', Point(x=0, y=4))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=4))},
            {('Q', Point(x=1, y=0)), ('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=4)), ('B', Point(x=0, y=3))},
            {('K', Point(x=0, y=4)), ('Q', Point(x=1, y=0)), ('N', Point(x=2, y=4)), ('Q', Point(x=4, y=1)), ('B', Point(x=3, y=4))},
            {('K', Point(x=0, y=4)), ('Q', Point(x=1, y=0)), ('B', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4))},
            {('K', Point(x=0, y=4)), ('Q', Point(x=1, y=0)), ('B', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4))},
            {('K', Point(x=0, y=4)), ('Q', Point(x=1, y=0)), ('B', Point(x=2, y=4)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4))},
            {('Q', Point(x=1, y=0)), ('K', Point(x=2, y=4)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=4)), ('B', Point(x=0, y=3))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('K', Point(x=3, y=4))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('B', Point(x=0, y=4)), ('K', Point(x=3, y=4))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('K', Point(x=3, y=4))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('B', Point(x=0, y=3)), ('K', Point(x=3, y=4))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=3, y=3)), ('B', Point(x=2, y=3)), ('K', Point(x=3, y=1))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=3, y=3)), ('B', Point(x=3, y=4)), ('K', Point(x=3, y=1))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('B', Point(x=3, y=3)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=1))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=1)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('B', Point(x=3, y=4)), ('K', Point(x=3, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('B', Point(x=3, y=3)), ('N', Point(x=3, y=4)), ('K', Point(x=4, y=1))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=4, y=1)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('B', Point(x=3, y=4)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=4, y=1)), ('K', Point(x=2, y=3)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=4, y=1)), ('K', Point(x=2, y=3)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=4, y=1)), ('K', Point(x=3, y=4)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=4, y=1)), ('K', Point(x=4, y=4)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=2, y=3))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=3, y=4))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=4)), ('B', Point(x=0, y=3)), ('Q', Point(x=4, y=2)), ('K', Point(x=3, y=4))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=1))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=1)), ('B', Point(x=4, y=4))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=3, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=3, y=4)), ('K', Point(x=3, y=1)), ('N', Point(x=4, y=4))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=3, y=4)), ('K', Point(x=4, y=1))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=3, y=4)), ('K', Point(x=4, y=1)), ('B', Point(x=4, y=4))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=3, y=4)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=4))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=4, y=1)), ('N', Point(x=4, y=2))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=4, y=1)), ('N', Point(x=4, y=4))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=4, y=2)), ('N', Point(x=4, y=4))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=2, y=4)), ('N', Point(x=4, y=4))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=3, y=4)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=3, y=4)), ('K', Point(x=4, y=2)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('N', Point(x=4, y=2)), ('B', Point(x=4, y=1))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('N', Point(x=4, y=2)), ('B', Point(x=2, y=2))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('N', Point(x=4, y=2)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('N', Point(x=4, y=4)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('B', Point(x=4, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=1)), ('K', Point(x=3, y=4)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=2)), ('K', Point(x=3, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=1)), ('K', Point(x=4, y=4)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=1)), ('K', Point(x=4, y=4)), ('B', Point(x=4, y=2))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=1)), ('K', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=4, y=1)), ('K', Point(x=4, y=4)), ('N', Point(x=4, y=2))},
            {('Q', Point(x=3, y=3)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=4)), ('B', Point(x=0, y=2)), ('K', Point(x=4, y=1))},
            {('Q', Point(x=3, y=3)), ('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('N', Point(x=0, y=4)), ('B', Point(x=4, y=1))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=2)), ('K', Point(x=2, y=3))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=1, y=0)), ('N', Point(x=4, y=1)), ('B', Point(x=3, y=1)), ('K', Point(x=0, y=3))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('Q', Point(x=3, y=4)), ('K', Point(x=4, y=1))},
            {('Q', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=2))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('Q', Point(x=3, y=4)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=2)), ('K', Point(x=0, y=3))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('B', Point(x=2, y=3)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=4))},
            {('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('K', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('B', Point(x=3, y=1))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('K', Point(x=2, y=3)), ('Q', Point(x=4, y=4))},
            {('K', Point(x=3, y=2)), ('Q', Point(x=2, y=0)), ('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=3, y=2)), ('Q', Point(x=2, y=0)), ('N', Point(x=1, y=4)), ('B', Point(x=4, y=4)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=3)), ('B', Point(x=3, y=2)), ('Q', Point(x=2, y=0)), ('K', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=1, y=3)), ('B', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=1, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=4, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=1, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=4, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=1, y=3)), ('N', Point(x=4, y=4)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=4, y=3)), ('Q', Point(x=2, y=0)), ('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=3)), ('K', Point(x=1, y=4)), ('Q', Point(x=2, y=0)), ('N', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=1, y=4)), ('Q', Point(x=2, y=0)), ('N', Point(x=4, y=3)), ('B', Point(x=4, y=4)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=1, y=4)), ('Q', Point(x=2, y=0)), ('B', Point(x=4, y=3)), ('N', Point(x=4, y=4)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=1, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=4)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=3)), ('K', Point(x=1, y=2)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=3, y=4))},
            {('K', Point(x=1, y=2)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=3, y=4))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=0, y=3))},
            {('N', Point(x=0, y=3)), ('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('B', Point(x=1, y=3))},
            {('N', Point(x=0, y=3)), ('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('B', Point(x=0, y=4))},
            {('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=1, y=3)), ('B', Point(x=1, y=2))},
            {('B', Point(x=0, y=3)), ('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=1, y=3))},
            {('B', Point(x=0, y=3)), ('K', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=0, y=4))},
            {('K', Point(x=0, y=4)), ('B', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=3, y=4))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('K', Point(x=3, y=4)), ('B', Point(x=1, y=3))},
            {('N', Point(x=0, y=3)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('K', Point(x=3, y=4))},
            {('B', Point(x=0, y=3)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('K', Point(x=3, y=4))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('B', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('B', Point(x=4, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('B', Point(x=4, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=4)), ('Q', Point(x=1, y=2))},
            {('B', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=4)), ('Q', Point(x=1, y=2))},
            {('K', Point(x=0, y=4)), ('B', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('K', Point(x=0, y=4)), ('B', Point(x=3, y=3)), ('Q', Point(x=2, y=0)), ('N', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('K', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('N', Point(x=4, y=3)), ('B', Point(x=4, y=4)), ('Q', Point(x=1, y=2))},
            {('K', Point(x=0, y=4)), ('B', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=4, y=4)), ('Q', Point(x=1, y=2))},
            {('K', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('B', Point(x=4, y=3)), ('N', Point(x=4, y=4)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=3, y=2)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('B', Point(x=1, y=3))},
            {('Q', Point(x=3, y=2)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('B', Point(x=0, y=4))},
            {('Q', Point(x=3, y=2)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=3, y=2)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('B', Point(x=0, y=3))},
            {('Q', Point(x=3, y=2)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=3, y=2)), ('N', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=4)), ('B', Point(x=0, y=1))},
            {('Q', Point(x=3, y=2)), ('N', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=4)), ('B', Point(x=1, y=3))},
            {('Q', Point(x=3, y=2)), ('N', Point(x=0, y=3)), ('B', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=4))},
            {('Q', Point(x=3, y=2)), ('N', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=4)), ('B', Point(x=0, y=1))},
            {('Q', Point(x=3, y=2)), ('N', Point(x=0, y=4)), ('B', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=4))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=0, y=3)), ('B', Point(x=3, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=0)), ('B', Point(x=4, y=4)), ('K', Point(x=3, y=2))},
            {('K', Point(x=0, y=1)), ('B', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('Q', Point(x=1, y=3)), ('N', Point(x=4, y=4))},
            {('B', Point(x=0, y=1)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('Q', Point(x=1, y=3)), ('N', Point(x=4, y=4))},
            {('B', Point(x=3, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('Q', Point(x=1, y=3)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('B', Point(x=4, y=1)), ('Q', Point(x=2, y=0))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('B', Point(x=1, y=4))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('B', Point(x=0, y=1))},
            {('K', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('B', Point(x=1, y=4))},
            {('K', Point(x=0, y=1)), ('B', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('N', Point(x=1, y=4))},
            {('K', Point(x=1, y=2)), ('B', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('N', Point(x=1, y=4))},
            {('Q', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=3)), ('B', Point(x=1, y=2))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=3))},
            {('K', Point(x=1, y=2)), ('Q', Point(x=0, y=4)), ('B', Point(x=4, y=1)), ('Q', Point(x=2, y=0)), ('N', Point(x=4, y=3))},
            {('B', Point(x=3, y=3)), ('K', Point(x=1, y=2)), ('Q', Point(x=0, y=4)), ('Q', Point(x=2, y=0)), ('N', Point(x=4, y=3))},
            {('B', Point(x=3, y=3)), ('K', Point(x=0, y=1)), ('Q', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=3, y=4)), ('Q', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('B', Point(x=1, y=3))},
            {('K', Point(x=0, y=1)), ('N', Point(x=0, y=3)), ('B', Point(x=3, y=2)), ('Q', Point(x=2, y=0)), ('Q', Point(x=4, y=4))},
            {('K', Point(x=0, y=1)), ('N', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('Q', Point(x=4, y=4)), ('B', Point(x=1, y=3))},
            {('B', Point(x=0, y=1)), ('N', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('Q', Point(x=4, y=4)), ('K', Point(x=3, y=2))},
            {('B', Point(x=1, y=3)), ('N', Point(x=0, y=3)), ('Q', Point(x=2, y=0)), ('Q', Point(x=4, y=4)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=2, y=2)), ('N', Point(x=2, y=4)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=2, y=2)), ('B', Point(x=4, y=2)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=2, y=2)), ('B', Point(x=4, y=3)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=2, y=2)), ('B', Point(x=1, y=4)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=2, y=2)), ('B', Point(x=2, y=4)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('B', Point(x=1, y=3)), ('N', Point(x=1, y=4)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('B', Point(x=4, y=4)), ('N', Point(x=1, y=4)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('N', Point(x=2, y=4)), ('B', Point(x=2, y=2)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('N', Point(x=2, y=4)), ('B', Point(x=1, y=4)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('N', Point(x=2, y=4)), ('B', Point(x=4, y=4)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('N', Point(x=4, y=4)), ('B', Point(x=1, y=3)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('N', Point(x=4, y=4)), ('B', Point(x=1, y=4)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=1, y=3)), ('N', Point(x=4, y=3)), ('B', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=1, y=3)), ('N', Point(x=4, y=3)), ('B', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=1, y=3)), ('B', Point(x=4, y=2)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=1, y=3)), ('B', Point(x=4, y=3)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('B', Point(x=2, y=4)), ('K', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=1, y=4)), ('B', Point(x=4, y=2)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('B', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=1, y=4)), ('B', Point(x=4, y=2)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=1, y=4)), ('B', Point(x=4, y=3)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=2, y=4)), ('B', Point(x=4, y=3)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=4, y=4)), ('N', Point(x=1, y=4)), ('B', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('K', Point(x=4, y=4)), ('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('B', Point(x=2, y=4)), ('K', Point(x=4, y=4)), ('N', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=1)), ('N', Point(x=2, y=4)), ('K', Point(x=4, y=4)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('N', Point(x=2, y=4)), ('B', Point(x=2, y=3)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('N', Point(x=2, y=4)), ('B', Point(x=0, y=4)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=4, y=2)), ('K', Point(x=2, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=0, y=4)), ('K', Point(x=2, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=2, y=4)), ('N', Point(x=0, y=4)), ('K', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=0, y=4)), ('B', Point(x=4, y=2)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=0, y=4)), ('B', Point(x=2, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=0, y=4)), ('N', Point(x=2, y=4)), ('B', Point(x=2, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=2, y=4)), ('N', Point(x=0, y=4)), ('B', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=4, y=4)), ('K', Point(x=2, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('B', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=1, y=4)), ('B', Point(x=4, y=3)), ('N', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=1)), ('Q', Point(x=4, y=2)), ('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=1)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('N', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=1, y=1)), ('B', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('N', Point(x=1, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=1, y=1)), ('Q', Point(x=4, y=2)), ('B', Point(x=1, y=4)), ('N', Point(x=1, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=1, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=1, y=1)), ('Q', Point(x=4, y=2)), ('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=1, y=1)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('N', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=1, y=1)), ('Q', Point(x=4, y=2)), ('K', Point(x=2, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=2, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=4)), ('N', Point(x=0, y=1)), ('B', Point(x=1, y=1)), ('Q', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=1, y=1)), ('K', Point(x=1, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=4, y=4)), ('K', Point(x=0, y=1)), ('B', Point(x=4, y=2)), ('Q', Point(x=1, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=4, y=2)), ('N', Point(x=4, y=4)), ('B', Point(x=0, y=1)), ('Q', Point(x=1, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('B', Point(x=2, y=4)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=1)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('B', Point(x=2, y=4)), ('K', Point(x=0, y=1)), ('Q', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=1, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('B', Point(x=2, y=4)), ('K', Point(x=1, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=1, y=1)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('B', Point(x=2, y=4)), ('K', Point(x=1, y=1)), ('Q', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=2)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=2, y=2)), ('N', Point(x=0, y=2)), ('B', Point(x=0, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=0, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=0, y=2)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=2, y=2)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=2, y=4)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=4)), ('N', Point(x=0, y=1)), ('B', Point(x=1, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=4)), ('N', Point(x=0, y=1)), ('B', Point(x=0, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=4)), ('N', Point(x=0, y=1)), ('B', Point(x=2, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=4)), ('N', Point(x=0, y=2)), ('B', Point(x=0, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=1, y=1)), ('K', Point(x=1, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=0, y=2)), ('K', Point(x=1, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=1, y=1)), ('K', Point(x=2, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('K', Point(x=2, y=4)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=2)), ('B', Point(x=0, y=1)), ('K', Point(x=2, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=2)), ('K', Point(x=2, y=4)), ('B', Point(x=2, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=2)), ('K', Point(x=2, y=4)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('B', Point(x=0, y=1)), ('K', Point(x=2, y=4)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('B', Point(x=1, y=1)), ('K', Point(x=2, y=4)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=4)), ('K', Point(x=1, y=1)), ('B', Point(x=4, y=2)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=4)), ('K', Point(x=1, y=1)), ('B', Point(x=2, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('B', Point(x=1, y=1)), ('Q', Point(x=0, y=4)), ('K', Point(x=2, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=0, y=4)), ('B', Point(x=4, y=2)), ('K', Point(x=2, y=3)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=1)), ('B', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=0, y=2)), ('B', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=0, y=2)), ('Q', Point(x=1, y=4)), ('K', Point(x=4, y=2)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=0, y=2)), ('K', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('Q', Point(x=3, y=0))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=0, y=1)), ('B', Point(x=1, y=1)), ('K', Point(x=4, y=3)), ('Q', Point(x=3, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=0, y=2)), ('K', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('Q', Point(x=3, y=0))},
            {('K', Point(x=3, y=2)), ('B', Point(x=2, y=4)), ('Q', Point(x=4, y=0)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=0, y=3)), ('Q', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=2, y=4)), ('Q', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=3, y=2)), ('Q', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=2, y=4)), ('Q', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=0, y=3))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=0, y=2)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1))},
            {('Q', Point(x=4, y=0)), ('K', Point(x=0, y=2)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1)), ('B', Point(x=1, y=4))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=3, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=2, y=1)), ('N', Point(x=3, y=4))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=2, y=1)), ('N', Point(x=3, y=4)), ('B', Point(x=1, y=4))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('N', Point(x=3, y=4)), ('B', Point(x=2, y=1))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=3, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('N', Point(x=3, y=4))},
            {('N', Point(x=0, y=1)), ('Q', Point(x=3, y=2)), ('Q', Point(x=4, y=0)), ('B', Point(x=1, y=1)), ('K', Point(x=0, y=3))},
            {('N', Point(x=0, y=1)), ('Q', Point(x=3, y=2)), ('Q', Point(x=4, y=0)), ('B', Point(x=2, y=4)), ('K', Point(x=0, y=3))},
            {('B', Point(x=1, y=1)), ('Q', Point(x=3, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=2, y=4)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=3, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=2, y=4)), ('B', Point(x=0, y=3)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=0, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=1, y=1)), ('N', Point(x=3, y=4)), ('B', Point(x=3, y=2))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=1, y=1)), ('N', Point(x=3, y=4))},
            {('B', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('Q', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=2))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('Q', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=3, y=3)), ('B', Point(x=0, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=2, y=1)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=2, y=1)), ('B', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('B', Point(x=2, y=1)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('B', Point(x=0, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=2, y=1)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=4, y=0)), ('K', Point(x=2, y=1)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=4, y=0)), ('K', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('B', Point(x=2, y=1)), ('N', Point(x=0, y=1))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=4, y=0)), ('K', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=0, y=1)), ('Q', Point(x=4, y=0)), ('B', Point(x=1, y=1)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=0, y=1)), ('Q', Point(x=4, y=0)), ('B', Point(x=0, y=3)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=0, y=1)), ('Q', Point(x=4, y=0)), ('B', Point(x=1, y=1)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=0, y=1)), ('Q', Point(x=4, y=0)), ('B', Point(x=3, y=2)), ('K', Point(x=0, y=3))},
            {('B', Point(x=0, y=2)), ('N', Point(x=0, y=3)), ('Q', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=1))},
            {('K', Point(x=3, y=0)), ('Q', Point(x=4, y=2)), ('N', Point(x=1, y=4)), ('B', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=2, y=0)), ('B', Point(x=3, y=2)), ('N', Point(x=4, y=2)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=2, y=0)), ('B', Point(x=4, y=4)), ('N', Point(x=4, y=2)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=4, y=4)), ('K', Point(x=3, y=0)), ('B', Point(x=4, y=2)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=3, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=3, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=4, y=4)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=4, y=2)), ('N', Point(x=4, y=4)), ('B', Point(x=3, y=0)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('K', Point(x=4, y=4)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('B', Point(x=3, y=2)), ('K', Point(x=4, y=4)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=2)), ('K', Point(x=4, y=4)), ('N', Point(x=4, y=2)), ('Q', Point(x=1, y=3)), ('Q', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('B', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('B', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=1, y=4)), ('B', Point(x=2, y=4)), ('Q', Point(x=4, y=3)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=2, y=4)), ('K', Point(x=3, y=0)), ('Q', Point(x=4, y=3)), ('N', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('K', Point(x=1, y=4)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('K', Point(x=2, y=4)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=4, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=4, y=0)), ('N', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=2, y=2)), ('N', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=4, y=3)), ('N', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=4, y=3)), ('B', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=4, y=3)), ('B', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=3, y=0)), ('B', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=3)), ('K', Point(x=3, y=0)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=4, y=0)), ('N', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('B', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=4, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=4, y=0)), ('B', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=4, y=0)), ('B', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=2, y=2)), ('N', Point(x=4, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=2, y=2)), ('B', Point(x=3, y=0)), ('N', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=2, y=2)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=2, y=2)), ('N', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('B', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=2, y=2)), ('N', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('B', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=0)), ('N', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('K', Point(x=4, y=2)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('K', Point(x=3, y=3)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('K', Point(x=3, y=3)), ('B', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=0)), ('K', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('K', Point(x=4, y=3)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('B', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('B', Point(x=3, y=0)), ('N', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('Q', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=3, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=4, y=0)), ('Q', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=3, y=0)), ('B', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('B', Point(x=3, y=0)), ('N', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=3, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=4)), ('Q', Point(x=0, y=1))},
            {('K', Point(x=3, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=1, y=3)), ('Q', Point(x=4, y=4)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('K', Point(x=1, y=3)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=4)), ('Q', Point(x=0, y=1))},
            {('N', Point(x=3, y=0)), ('B', Point(x=3, y=2)), ('K', Point(x=1, y=3)), ('Q', Point(x=4, y=4)), ('Q', Point(x=0, y=1))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=3, y=2)), ('K', Point(x=4, y=0)), ('N', Point(x=0, y=4)), ('B', Point(x=0, y=3))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=3, y=2)), ('K', Point(x=4, y=0)), ('N', Point(x=0, y=4)), ('B', Point(x=2, y=4))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=3, y=2)), ('K', Point(x=2, y=4)), ('N', Point(x=0, y=4)), ('B', Point(x=0, y=3))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=3, y=0)), ('N', Point(x=0, y=4)), ('B', Point(x=3, y=4)), ('Q', Point(x=4, y=2))},
            {('Q', Point(x=1, y=1)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=3, y=4)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=0, y=3)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=3, y=4))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('K', Point(x=4, y=0)), ('B', Point(x=3, y=2)), ('N', Point(x=4, y=2))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('K', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('B', Point(x=3, y=2))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('K', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('B', Point(x=2, y=4))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('B', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('B', Point(x=2, y=4)), ('N', Point(x=3, y=4)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('B', Point(x=3, y=4)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('B', Point(x=4, y=0)), ('N', Point(x=4, y=2))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('B', Point(x=3, y=2)), ('N', Point(x=4, y=2))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=2)), ('K', Point(x=3, y=4))},
            {('Q', Point(x=1, y=1)), ('N', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=2)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=0, y=4)), ('N', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=0, y=4)), ('N', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('B', Point(x=4, y=2))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=3, y=0)), ('N', Point(x=0, y=4)), ('B', Point(x=2, y=4)), ('Q', Point(x=4, y=3))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=2, y=4)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=3)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=3, y=0)), ('Q', Point(x=0, y=4)), ('B', Point(x=4, y=2)), ('N', Point(x=4, y=3))},
            {('Q', Point(x=1, y=1)), ('K', Point(x=3, y=0)), ('Q', Point(x=0, y=4)), ('B', Point(x=2, y=3)), ('N', Point(x=4, y=3))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=4)), ('K', Point(x=2, y=3)), ('N', Point(x=4, y=3)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=0, y=4)), ('B', Point(x=4, y=2)), ('K', Point(x=2, y=3)), ('N', Point(x=4, y=3))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=2, y=4)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=3)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=2, y=4)), ('N', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=1, y=1)), ('Q', Point(x=2, y=4)), ('N', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('B', Point(x=0, y=3))},
            {('Q', Point(x=1, y=1)), ('B', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=1, y=1)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=2)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=4, y=0)), ('B', Point(x=3, y=3)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1)), ('B', Point(x=1, y=4))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=4, y=0)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=4, y=0)), ('Q', Point(x=2, y=1)), ('N', Point(x=4, y=4)), ('B', Point(x=1, y=4))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=4, y=0)), ('B', Point(x=3, y=4)), ('Q', Point(x=2, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=3, y=3)), ('K', Point(x=1, y=4)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=1, y=4)), ('N', Point(x=3, y=4)), ('Q', Point(x=2, y=1)), ('B', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('Q', Point(x=2, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=1, y=4)), ('B', Point(x=3, y=4)), ('Q', Point(x=2, y=1)), ('N', Point(x=4, y=4))},
            {('N', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=0, y=0)), ('B', Point(x=1, y=4))},
            {('N', Point(x=0, y=4)), ('B', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=0, y=0))},
            {('B', Point(x=1, y=3)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=0, y=0)), ('N', Point(x=1, y=4))},
            {('N', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=3, y=4)), ('B', Point(x=0, y=0))},
            {('N', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=3, y=4)), ('B', Point(x=1, y=4))},
            {('B', Point(x=0, y=0)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=3, y=4)), ('N', Point(x=1, y=4))},
            {('B', Point(x=1, y=3)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=3, y=4)), ('N', Point(x=1, y=4))},
            {('B', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=2, y=1)), ('K', Point(x=3, y=4)), ('N', Point(x=1, y=4))},
            {('B', Point(x=4, y=2)), ('K', Point(x=0, y=0)), ('Q', Point(x=2, y=1)), ('Q', Point(x=1, y=3)), ('N', Point(x=4, y=4))},
            {('B', Point(x=3, y=4)), ('K', Point(x=0, y=0)), ('Q', Point(x=2, y=1)), ('Q', Point(x=1, y=3)), ('N', Point(x=4, y=4))},
            {('N', Point(x=4, y=4)), ('B', Point(x=3, y=4)), ('Q', Point(x=2, y=1)), ('Q', Point(x=1, y=3)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=4, y=0)), ('N', Point(x=0, y=4)), ('B', Point(x=0, y=2)), ('Q', Point(x=2, y=1))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=4, y=0)), ('N', Point(x=0, y=4)), ('Q', Point(x=2, y=1)), ('B', Point(x=1, y=4))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=2)), ('N', Point(x=0, y=4)), ('Q', Point(x=2, y=1)), ('B', Point(x=1, y=4))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=4, y=4)), ('B', Point(x=1, y=4)), ('Q', Point(x=3, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=1, y=0)), ('K', Point(x=1, y=4)), ('Q', Point(x=3, y=1)), ('N', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=1, y=4)), ('B', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=4, y=4))},
            {('N', Point(x=4, y=4)), ('K', Point(x=0, y=0)), ('B', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('Q', Point(x=1, y=2))},
            {('N', Point(x=4, y=4)), ('B', Point(x=2, y=4)), ('K', Point(x=0, y=0)), ('Q', Point(x=3, y=1)), ('Q', Point(x=1, y=2))},
            {('N', Point(x=4, y=4)), ('K', Point(x=2, y=4)), ('B', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('Q', Point(x=1, y=2))},
            {('N', Point(x=4, y=4)), ('Q', Point(x=0, y=3)), ('B', Point(x=2, y=4)), ('Q', Point(x=3, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('B', Point(x=1, y=0)), ('Q', Point(x=3, y=1)), ('N', Point(x=4, y=4))},
            {('B', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=2, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=0, y=0))},
            {('B', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=4)), ('Q', Point(x=3, y=1)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=2)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=4)), ('Q', Point(x=3, y=1)), ('N', Point(x=0, y=0))},
            {('N', Point(x=0, y=2)), ('K', Point(x=0, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('B', Point(x=1, y=2))},
            {('B', Point(x=1, y=2)), ('K', Point(x=0, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=1, y=4))},
            {('B', Point(x=2, y=4)), ('K', Point(x=0, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=1, y=4))},
            {('B', Point(x=1, y=4)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=0)), ('K', Point(x=1, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=1, y=4))},
            {('B', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=1, y=4))},
            {('B', Point(x=0, y=2)), ('K', Point(x=1, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=0, y=0))},
            {('N', Point(x=0, y=2)), ('K', Point(x=2, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('B', Point(x=0, y=0))},
            {('N', Point(x=0, y=2)), ('K', Point(x=2, y=4)), ('Q', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('B', Point(x=1, y=2))},
            {('B', Point(x=4, y=3)), ('K', Point(x=0, y=2)), ('Q', Point(x=1, y=4)), ('Q', Point(x=3, y=1)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=2)), ('K', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('Q', Point(x=3, y=1)), ('N', Point(x=0, y=0))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=0, y=0)), ('B', Point(x=1, y=0)), ('Q', Point(x=3, y=1)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=0, y=0)), ('B', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=2, y=4)), ('B', Point(x=0, y=3)), ('K', Point(x=4, y=3)), ('Q', Point(x=3, y=1)), ('N', Point(x=0, y=0))},
            {('K', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('B', Point(x=0, y=2)), ('Q', Point(x=3, y=1)), ('Q', Point(x=4, y=4))},
            {('K', Point(x=1, y=0)), ('N', Point(x=0, y=3)), ('B', Point(x=2, y=3)), ('Q', Point(x=3, y=1)), ('Q', Point(x=4, y=4))},
            {('N', Point(x=0, y=3)), ('B', Point(x=1, y=0)), ('K', Point(x=2, y=3)), ('Q', Point(x=3, y=1)), ('Q', Point(x=4, y=4))},
            {('B', Point(x=0, y=2)), ('N', Point(x=0, y=3)), ('K', Point(x=2, y=3)), ('Q', Point(x=3, y=1)), ('Q', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=3, y=3)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4)), ('K', Point(x=1, y=0))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4)), ('K', Point(x=1, y=0))},
            {('B', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('N', Point(x=3, y=4)), ('K', Point(x=2, y=0))},
            {('Q', Point(x=0, y=3)), ('K', Point(x=2, y=4)), ('Q', Point(x=4, y=1)), ('N', Point(x=1, y=0)), ('B', Point(x=2, y=0))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('B', Point(x=2, y=0)), ('K', Point(x=3, y=4))},
            {('N', Point(x=0, y=0)), ('Q', Point(x=4, y=1)), ('B', Point(x=3, y=4)), ('Q', Point(x=1, y=3)), ('K', Point(x=2, y=0))},
            {('Q', Point(x=4, y=1)), ('B', Point(x=2, y=0)), ('K', Point(x=3, y=4)), ('Q', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('Q', Point(x=3, y=3)), ('B', Point(x=0, y=2)), ('N', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('K', Point(x=2, y=0)), ('B', Point(x=1, y=2))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=0, y=2)), ('B', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('K', Point(x=2, y=0))},
            {('Q', Point(x=3, y=3)), ('B', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=4))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=1, y=2)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('B', Point(x=2, y=0))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=1, y=2)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('B', Point(x=0, y=4))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('B', Point(x=2, y=0))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('B', Point(x=1, y=2))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=4)), ('N', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('B', Point(x=1, y=2))},
            {('K', Point(x=1, y=2)), ('Q', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('N', Point(x=1, y=0)), ('B', Point(x=2, y=0))},
            {('B', Point(x=3, y=3)), ('K', Point(x=1, y=2)), ('Q', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('N', Point(x=1, y=0))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('K', Point(x=3, y=3)), ('B', Point(x=2, y=0))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('Q', Point(x=4, y=1)), ('K', Point(x=3, y=3)), ('B', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('B', Point(x=0, y=0))},
            {('Q', Point(x=2, y=4)), ('B', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('N', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('B', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('N', Point(x=0, y=2)), ('B', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=0, y=0))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=0, y=0)), ('B', Point(x=2, y=0))},
            {('B', Point(x=0, y=2)), ('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=0, y=0))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=0, y=0)), ('B', Point(x=1, y=3))},
            {('B', Point(x=0, y=2)), ('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=1, y=0))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('B', Point(x=1, y=3)), ('K', Point(x=1, y=0))},
            {('B', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=0)), ('N', Point(x=0, y=0))},
            {('B', Point(x=1, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=0)), ('N', Point(x=0, y=0))},
            {('N', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=0)), ('B', Point(x=0, y=0))},
            {('N', Point(x=0, y=2)), ('B', Point(x=2, y=2)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=0))},
            {('N', Point(x=0, y=2)), ('B', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=0))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=0)), ('B', Point(x=0, y=0))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=0)), ('B', Point(x=1, y=3))},
            {('B', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('B', Point(x=2, y=0)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=0, y=2)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('N', Point(x=0, y=2)), ('B', Point(x=0, y=3)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4))},
            {('B', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('B', Point(x=2, y=0)), ('N', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('B', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('B', Point(x=2, y=0)), ('K', Point(x=0, y=3))},
            {('B', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=1, y=3)), ('N', Point(x=0, y=0)), ('B', Point(x=2, y=0))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=1, y=3)), ('B', Point(x=0, y=0))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=4, y=1)), ('Q', Point(x=3, y=4)), ('K', Point(x=1, y=3)), ('B', Point(x=2, y=0))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=3, y=0)), ('Q', Point(x=2, y=3)), ('B', Point(x=3, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=3, y=0)), ('Q', Point(x=2, y=3)), ('B', Point(x=4, y=4)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=3, y=0)), ('N', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('B', Point(x=4, y=4)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=4)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=4)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=4)), ('B', Point(x=3, y=1)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=1, y=0)), ('N', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=4))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=4, y=0)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=4)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('B', Point(x=1, y=4)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('Q', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('K', Point(x=1, y=4)), ('B', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=1, y=4)), ('Q', Point(x=4, y=3)), ('B', Point(x=3, y=1)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=3, y=0)), ('B', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=4, y=0)), ('B', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=4, y=0)), ('K', Point(x=2, y=1)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=3, y=3)), ('K', Point(x=2, y=1)), ('Q', Point(x=1, y=4)), ('N', Point(x=4, y=3))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=3, y=3)), ('B', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('B', Point(x=3, y=1)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=1)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=3, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=1)), ('B', Point(x=2, y=1))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('K', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('B', Point(x=3, y=1)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('K', Point(x=2, y=1)), ('N', Point(x=4, y=1))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=3, y=1)), ('Q', Point(x=4, y=4)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=1, y=0)), ('K', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=0, y=2)), ('B', Point(x=3, y=1)), ('K', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=1)), ('K', Point(x=2, y=0)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=4)), ('N', Point(x=4, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=4)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=1)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=0, y=4)), ('B', Point(x=4, y=1)), ('N', Point(x=4, y=3)), ('K', Point(x=2, y=0)), ('Q', Point(x=1, y=2))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=0, y=4)), ('N', Point(x=4, y=3)), ('K', Point(x=2, y=0)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=0, y=4)), ('B', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('B', Point(x=3, y=3)), ('Q', Point(x=0, y=4)), ('K', Point(x=4, y=1)), ('N', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=1)), ('K', Point(x=0, y=0)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=4, y=0)), ('K', Point(x=0, y=0)), ('B', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('B', Point(x=4, y=0)), ('N', Point(x=4, y=1)), ('K', Point(x=0, y=0)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=4, y=1)), ('K', Point(x=0, y=0)), ('B', Point(x=3, y=1)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=4, y=1)), ('K', Point(x=0, y=0)), ('B', Point(x=4, y=3)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('B', Point(x=0, y=0)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('B', Point(x=4, y=1)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=4, y=3)), ('N', Point(x=4, y=1)), ('B', Point(x=0, y=0)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('B', Point(x=4, y=0)), ('K', Point(x=4, y=3)), ('N', Point(x=4, y=1)), ('Q', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=4, y=3)), ('N', Point(x=4, y=1)), ('B', Point(x=3, y=1)), ('Q', Point(x=1, y=2))},
            {('N', Point(x=0, y=0)), ('Q', Point(x=3, y=2)), ('B', Point(x=0, y=1)), ('Q', Point(x=1, y=3)), ('K', Point(x=2, y=0))},
            {('Q', Point(x=3, y=2)), ('K', Point(x=4, y=4)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=1)), ('Q', Point(x=3, y=2)), ('K', Point(x=4, y=4)), ('Q', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('K', Point(x=4, y=0)), ('N', Point(x=0, y=0)), ('B', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('K', Point(x=4, y=0)), ('B', Point(x=0, y=3)), ('N', Point(x=0, y=0))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('K', Point(x=4, y=0)), ('B', Point(x=0, y=0)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('K', Point(x=4, y=0)), ('B', Point(x=1, y=1)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('K', Point(x=4, y=0)), ('B', Point(x=0, y=3)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('B', Point(x=4, y=0)), ('N', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('B', Point(x=0, y=1)), ('N', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('N', Point(x=0, y=1)), ('B', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('N', Point(x=0, y=1)), ('B', Point(x=4, y=0)), ('K', Point(x=0, y=3))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=3, y=2)), ('N', Point(x=0, y=1)), ('B', Point(x=1, y=1)), ('K', Point(x=0, y=3))},
            {('B', Point(x=0, y=1)), ('Q', Point(x=3, y=2)), ('N', Point(x=0, y=3)), ('Q', Point(x=4, y=4)), ('K', Point(x=2, y=0))},
            {('B', Point(x=1, y=3)), ('Q', Point(x=3, y=2)), ('N', Point(x=0, y=3)), ('Q', Point(x=4, y=4)), ('K', Point(x=2, y=0))},
            {('Q', Point(x=3, y=2)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=3)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=4))},
            {('Q', Point(x=3, y=2)), ('K', Point(x=0, y=1)), ('N', Point(x=0, y=3)), ('Q', Point(x=4, y=4)), ('B', Point(x=1, y=3))},
            {('B', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=2)), ('K', Point(x=3, y=4))},
            {('B', Point(x=0, y=1)), ('K', Point(x=3, y=0)), ('Q', Point(x=4, y=2)), ('Q', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('K', Point(x=3, y=0)), ('B', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('Q', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('B', Point(x=3, y=0)), ('Q', Point(x=4, y=2)), ('K', Point(x=3, y=4)), ('Q', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('K', Point(x=3, y=0)), ('B', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('N', Point(x=0, y=0))},
            {('K', Point(x=3, y=0)), ('B', Point(x=0, y=4)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('N', Point(x=0, y=0))},
            {('K', Point(x=3, y=0)), ('N', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('B', Point(x=0, y=0))},
            {('B', Point(x=1, y=1)), ('K', Point(x=3, y=0)), ('N', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2))},
            {('K', Point(x=3, y=0)), ('N', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('B', Point(x=0, y=4))},
            {('K', Point(x=0, y=4)), ('B', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('N', Point(x=0, y=0))},
            {('K', Point(x=0, y=4)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('N', Point(x=0, y=0)), ('B', Point(x=3, y=0))},
            {('K', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('B', Point(x=0, y=0))},
            {('K', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2)), ('B', Point(x=3, y=0))},
            {('K', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('B', Point(x=1, y=1)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=2))},
            {('B', Point(x=1, y=1)), ('K', Point(x=3, y=0)), ('Q', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=2))},
            {('K', Point(x=3, y=0)), ('Q', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('Q', Point(x=4, y=2)), ('B', Point(x=2, y=3))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=2, y=3)), ('B', Point(x=3, y=0))},
            {('B', Point(x=1, y=1)), ('N', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=2, y=3))},
            {('K', Point(x=3, y=0)), ('Q', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0)), ('B', Point(x=0, y=1))},
            {('K', Point(x=3, y=0)), ('Q', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('B', Point(x=0, y=0)), ('N', Point(x=0, y=1))},
            {('K', Point(x=2, y=1)), ('Q', Point(x=4, y=2)), ('Q', Point(x=1, y=4)), ('B', Point(x=0, y=0)), ('N', Point(x=0, y=1))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=0, y=0)), ('B', Point(x=1, y=3))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('B', Point(x=1, y=3)), ('K', Point(x=1, y=0))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=2, y=1)), ('B', Point(x=0, y=0))},
            {('N', Point(x=0, y=3)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=2, y=1)), ('B', Point(x=1, y=3))},
            {('B', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('N', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('B', Point(x=0, y=0)), ('K', Point(x=0, y=3))},
            {('B', Point(x=1, y=1)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=0, y=3))},
            {('B', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=1, y=3)), ('B', Point(x=0, y=0))},
            {('B', Point(x=1, y=1)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=1, y=3))},
            {('N', Point(x=1, y=1)), ('B', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=1, y=3))},
            {('N', Point(x=1, y=1)), ('Q', Point(x=3, y=4)), ('Q', Point(x=4, y=2)), ('K', Point(x=1, y=3)), ('B', Point(x=2, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('B', Point(x=4, y=0)), ('N', Point(x=4, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=1)), ('B', Point(x=3, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('K', Point(x=4, y=0)), ('N', Point(x=1, y=0)), ('B', Point(x=1, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('B', Point(x=4, y=1)), ('N', Point(x=3, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('B', Point(x=3, y=2)), ('N', Point(x=3, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=0))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('N', Point(x=4, y=1)), ('B', Point(x=3, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('B', Point(x=1, y=1)), ('K', Point(x=4, y=1))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('B', Point(x=4, y=0)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=2, y=4)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('B', Point(x=1, y=1)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=2)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=3)), ('B', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=1)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=3)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=2)), ('K', Point(x=1, y=0))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=1)), ('K', Point(x=2, y=0))},
            {('K', Point(x=4, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=2, y=0))},
            {('K', Point(x=4, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=1, y=1))},
            {('K', Point(x=4, y=0)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=2))},
            {('K', Point(x=4, y=0)), ('Q', Point(x=0, y=3)), ('B', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('N', Point(x=2, y=0))},
            {('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=1))},
            {('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=0)), ('B', Point(x=4, y=2))},
            {('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=0))},
            {('Q', Point(x=0, y=3)), ('K', Point(x=1, y=1)), ('Q', Point(x=3, y=4)), ('N', Point(x=4, y=1)), ('B', Point(x=4, y=2))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=2, y=0)), ('K', Point(x=4, y=1))},
            {('B', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('K', Point(x=4, y=1))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=0, y=3)), ('B', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('N', Point(x=2, y=0))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=1, y=0))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=2, y=0))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=1))},
            {('K', Point(x=2, y=2)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=2))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=0)), ('K', Point(x=4, y=2))},
            {('B', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=0, y=3)), ('B', Point(x=1, y=0)), ('Q', Point(x=3, y=4)), ('N', Point(x=2, y=0)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=0, y=3)), ('B', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('N', Point(x=2, y=0)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=0, y=3)), ('B', Point(x=2, y=2)), ('Q', Point(x=3, y=4)), ('N', Point(x=2, y=0)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=1, y=0)), ('K', Point(x=4, y=2))},
            {('B', Point(x=1, y=1)), ('Q', Point(x=0, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=3, y=4)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=0, y=3)), ('N', Point(x=1, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=4)), ('K', Point(x=3, y=2))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=0, y=0)), ('N', Point(x=2, y=0)), ('Q', Point(x=1, y=3)), ('B', Point(x=2, y=1))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=0, y=0)), ('N', Point(x=4, y=1)), ('Q', Point(x=1, y=3)), ('B', Point(x=2, y=1))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=0, y=0)), ('N', Point(x=4, y=1)), ('Q', Point(x=1, y=3)), ('B', Point(x=4, y=2))},
            {('N', Point(x=0, y=0)), ('Q', Point(x=3, y=4)), ('B', Point(x=4, y=1)), ('Q', Point(x=1, y=3)), ('K', Point(x=2, y=0))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=2, y=1)), ('N', Point(x=4, y=1)), ('Q', Point(x=1, y=3)), ('B', Point(x=0, y=0))},
            {('Q', Point(x=3, y=4)), ('K', Point(x=2, y=1)), ('N', Point(x=4, y=1)), ('Q', Point(x=1, y=3)), ('B', Point(x=4, y=2))},
            {('Q', Point(x=3, y=4)), ('B', Point(x=2, y=0)), ('K', Point(x=4, y=1)), ('Q', Point(x=1, y=3)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=0)), ('Q', Point(x=3, y=4)), ('N', Point(x=2, y=0)), ('Q', Point(x=1, y=3)), ('K', Point(x=4, y=2))},
            {('Q', Point(x=3, y=4)), ('N', Point(x=2, y=0)), ('B', Point(x=2, y=1)), ('Q', Point(x=1, y=3)), ('K', Point(x=4, y=2))},
            {('N', Point(x=3, y=0)), ('K', Point(x=0, y=1)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=3)), ('Q', Point(x=4, y=4))},
            {('N', Point(x=3, y=0)), ('K', Point(x=0, y=1)), ('B', Point(x=3, y=2)), ('Q', Point(x=1, y=3)), ('Q', Point(x=4, y=4))},
            {('K', Point(x=3, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=3)), ('Q', Point(x=4, y=4))},
            {('K', Point(x=3, y=2)), ('N', Point(x=3, y=0)), ('B', Point(x=0, y=1)), ('Q', Point(x=1, y=3)), ('Q', Point(x=4, y=4))},
            {('B', Point(x=1, y=1)), ('K', Point(x=3, y=0)), ('Q', Point(x=0, y=4)), ('Q', Point(x=2, y=3)), ('N', Point(x=1, y=0))},
            {('K', Point(x=3, y=0)), ('Q', Point(x=0, y=4)), ('Q', Point(x=2, y=3)), ('N', Point(x=1, y=0)), ('B', Point(x=4, y=2))},
            {('N', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=2)), ('B', Point(x=3, y=0))},
            {('B', Point(x=1, y=1)), ('N', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('Q', Point(x=2, y=3)), ('K', Point(x=4, y=2))},
            {('N', Point(x=3, y=0)), ('B', Point(x=3, y=1)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('K', Point(x=1, y=0))},
            {('N', Point(x=3, y=0)), ('B', Point(x=0, y=2)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('K', Point(x=1, y=0))},
            {('B', Point(x=1, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('N', Point(x=3, y=0))},
            {('B', Point(x=3, y=1)), ('K', Point(x=0, y=2)), ('Q', Point(x=2, y=3)), ('Q', Point(x=4, y=4)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('B', Point(x=2, y=0)), ('K', Point(x=4, y=1))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=1, y=0)), ('Q', Point(x=0, y=4)), ('K', Point(x=4, y=1)), ('B', Point(x=1, y=2))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=1, y=2)), ('Q', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('B', Point(x=2, y=0))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=1, y=2)), ('Q', Point(x=0, y=4)), ('N', Point(x=1, y=0)), ('B', Point(x=4, y=1))},
            {('Q', Point(x=3, y=3)), ('N', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0)), ('B', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=4, y=0)), ('N', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('B', Point(x=2, y=1))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=4, y=0)), ('Q', Point(x=1, y=4)), ('B', Point(x=2, y=1)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=4, y=0)), ('B', Point(x=0, y=2)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=1)), ('N', Point(x=4, y=0)), ('B', Point(x=2, y=0)), ('Q', Point(x=1, y=4))},
            {('Q', Point(x=3, y=3)), ('B', Point(x=4, y=0)), ('K', Point(x=2, y=1)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('B', Point(x=0, y=2)), ('K', Point(x=2, y=1)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=2)), ('B', Point(x=4, y=0)), ('N', Point(x=2, y=0)), ('Q', Point(x=1, y=4))},
            {('Q', Point(x=3, y=3)), ('K', Point(x=0, y=2)), ('N', Point(x=2, y=0)), ('Q', Point(x=1, y=4)), ('B', Point(x=2, y=1))},
            {('K', Point(x=1, y=2)), ('Q', Point(x=0, y=4)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('N', Point(x=3, y=0))},
            {('B', Point(x=3, y=0)), ('K', Point(x=0, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=2, y=0))},
            {('B', Point(x=2, y=0)), ('K', Point(x=0, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('B', Point(x=3, y=1)), ('K', Point(x=0, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('B', Point(x=0, y=2)), ('K', Point(x=0, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('N', Point(x=0, y=0)), ('B', Point(x=0, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('K', Point(x=2, y=0))},
            {('B', Point(x=0, y=1)), ('K', Point(x=3, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=2)), ('K', Point(x=3, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=0)), ('K', Point(x=3, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('B', Point(x=0, y=2)), ('K', Point(x=3, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('K', Point(x=0, y=1)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('B', Point(x=3, y=1)), ('K', Point(x=0, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('B', Point(x=0, y=1)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=2)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=0)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('B', Point(x=0, y=2)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=1))},
            {('B', Point(x=3, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('B', Point(x=3, y=1)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=0, y=2)), ('N', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('B', Point(x=0, y=0))},
            {('K', Point(x=0, y=2)), ('N', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('B', Point(x=3, y=0))},
            {('K', Point(x=0, y=2)), ('B', Point(x=2, y=2)), ('N', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4))},
            {('B', Point(x=0, y=0)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('B', Point(x=3, y=1)), ('K', Point(x=0, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=3, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=3, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=0, y=1)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('B', Point(x=0, y=2)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('N', Point(x=0, y=0))},
            {('K', Point(x=2, y=2)), ('N', Point(x=2, y=0)), ('Q', Point(x=4, y=3)), ('Q', Point(x=1, y=4)), ('B', Point(x=3, y=0))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=0, y=0)), ('Q', Point(x=4, y=3)), ('B', Point(x=3, y=1)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=3, y=0)), ('Q', Point(x=4, y=3)), ('N', Point(x=0, y=0)), ('B', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=3, y=0)), ('Q', Point(x=4, y=3)), ('B', Point(x=0, y=0)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=3, y=0)), ('B', Point(x=1, y=1)), ('Q', Point(x=4, y=3)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=0, y=1)), ('Q', Point(x=4, y=3)), ('B', Point(x=3, y=1)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('N', Point(x=0, y=0)), ('B', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('B', Point(x=0, y=0)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('B', Point(x=1, y=1)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('N', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=1, y=1)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('B', Point(x=0, y=1))},
            {('Q', Point(x=2, y=4)), ('N', Point(x=1, y=1)), ('K', Point(x=3, y=1)), ('Q', Point(x=4, y=3)), ('B', Point(x=1, y=2))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('Q', Point(x=4, y=3)), ('B', Point(x=0, y=0)), ('N', Point(x=3, y=0))},
            {('Q', Point(x=2, y=4)), ('K', Point(x=1, y=2)), ('Q', Point(x=4, y=3)), ('B', Point(x=3, y=1)), ('N', Point(x=3, y=0))},
        ]

        self.assertEqual(len(result), len(expected))

        for item in expected:
            with self.subTest(item=item):
                self.assertIn(item, result)

    def test_5x5_b1_n1(self):
        solver = ChessProblemSolver(BoardSize(5, 5), {'B': 1, 'N': 1})
        result = [set(i) for i in solver.solve()]

        expected = [
            {('B', Point(x=1, y=0)), ('N', Point(x=0, y=0))},
            {('N', Point(x=0, y=0)), ('B', Point(x=2, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=0, y=0))},
            {('B', Point(x=4, y=0)), ('N', Point(x=0, y=0))},
            {('B', Point(x=0, y=1)), ('N', Point(x=0, y=0))},
            {('N', Point(x=0, y=0)), ('B', Point(x=3, y=1))},
            {('N', Point(x=0, y=0)), ('B', Point(x=4, y=1))},
            {('N', Point(x=0, y=0)), ('B', Point(x=0, y=2))},
            {('B', Point(x=3, y=2)), ('N', Point(x=0, y=0))},
            {('N', Point(x=0, y=0)), ('B', Point(x=4, y=2))},
            {('B', Point(x=0, y=3)), ('N', Point(x=0, y=0))},
            {('N', Point(x=0, y=0)), ('B', Point(x=1, y=3))},
            {('N', Point(x=0, y=0)), ('B', Point(x=2, y=3))},
            {('N', Point(x=0, y=0)), ('B', Point(x=4, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=0, y=0))},
            {('N', Point(x=0, y=0)), ('B', Point(x=1, y=4))},
            {('N', Point(x=0, y=0)), ('B', Point(x=2, y=4))},
            {('N', Point(x=0, y=0)), ('B', Point(x=3, y=4))},
            {('N', Point(x=1, y=0)), ('B', Point(x=0, y=0))},
            {('N', Point(x=1, y=0)), ('B', Point(x=2, y=0))},
            {('N', Point(x=1, y=0)), ('B', Point(x=3, y=0))},
            {('N', Point(x=1, y=0)), ('B', Point(x=4, y=0))},
            {('N', Point(x=1, y=0)), ('B', Point(x=1, y=1))},
            {('N', Point(x=1, y=0)), ('B', Point(x=4, y=1))},
            {('N', Point(x=1, y=0)), ('B', Point(x=1, y=2))},
            {('N', Point(x=1, y=0)), ('B', Point(x=4, y=2))},
            {('N', Point(x=1, y=0)), ('B', Point(x=0, y=3))},
            {('N', Point(x=1, y=0)), ('B', Point(x=1, y=3))},
            {('N', Point(x=1, y=0)), ('B', Point(x=2, y=3))},
            {('N', Point(x=1, y=0)), ('B', Point(x=3, y=3))},
            {('N', Point(x=1, y=0)), ('B', Point(x=0, y=4))},
            {('N', Point(x=1, y=0)), ('B', Point(x=1, y=4))},
            {('N', Point(x=1, y=0)), ('B', Point(x=2, y=4))},
            {('N', Point(x=1, y=0)), ('B', Point(x=3, y=4))},
            {('N', Point(x=1, y=0)), ('B', Point(x=4, y=4))},
            {('N', Point(x=2, y=0)), ('B', Point(x=0, y=0))},
            {('B', Point(x=1, y=0)), ('N', Point(x=2, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=2, y=0))},
            {('B', Point(x=4, y=0)), ('N', Point(x=2, y=0))},
            {('N', Point(x=2, y=0)), ('B', Point(x=2, y=1))},
            {('B', Point(x=2, y=2)), ('N', Point(x=2, y=0))},
            {('B', Point(x=0, y=3)), ('N', Point(x=2, y=0))},
            {('N', Point(x=2, y=0)), ('B', Point(x=1, y=3))},
            {('B', Point(x=2, y=3)), ('N', Point(x=2, y=0))},
            {('N', Point(x=2, y=0)), ('B', Point(x=3, y=3))},
            {('N', Point(x=2, y=0)), ('B', Point(x=4, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=2, y=0))},
            {('N', Point(x=2, y=0)), ('B', Point(x=1, y=4))},
            {('N', Point(x=2, y=0)), ('B', Point(x=2, y=4))},
            {('B', Point(x=3, y=4)), ('N', Point(x=2, y=0))},
            {('N', Point(x=2, y=0)), ('B', Point(x=4, y=4))},
            {('N', Point(x=3, y=0)), ('B', Point(x=0, y=0))},
            {('N', Point(x=3, y=0)), ('B', Point(x=1, y=0))},
            {('N', Point(x=3, y=0)), ('B', Point(x=2, y=0))},
            {('B', Point(x=4, y=0)), ('N', Point(x=3, y=0))},
            {('B', Point(x=0, y=1)), ('N', Point(x=3, y=0))},
            {('N', Point(x=3, y=0)), ('B', Point(x=3, y=1))},
            {('N', Point(x=3, y=0)), ('B', Point(x=0, y=2))},
            {('N', Point(x=3, y=0)), ('B', Point(x=3, y=2))},
            {('N', Point(x=3, y=0)), ('B', Point(x=1, y=3))},
            {('N', Point(x=3, y=0)), ('B', Point(x=2, y=3))},
            {('N', Point(x=3, y=0)), ('B', Point(x=3, y=3))},
            {('N', Point(x=3, y=0)), ('B', Point(x=4, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=3, y=0))},
            {('N', Point(x=3, y=0)), ('B', Point(x=1, y=4))},
            {('N', Point(x=3, y=0)), ('B', Point(x=2, y=4))},
            {('N', Point(x=3, y=0)), ('B', Point(x=3, y=4))},
            {('N', Point(x=3, y=0)), ('B', Point(x=4, y=4))},
            {('B', Point(x=0, y=0)), ('N', Point(x=4, y=0))},
            {('B', Point(x=1, y=0)), ('N', Point(x=4, y=0))},
            {('B', Point(x=2, y=0)), ('N', Point(x=4, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=4, y=0))},
            {('B', Point(x=0, y=1)), ('N', Point(x=4, y=0))},
            {('B', Point(x=1, y=1)), ('N', Point(x=4, y=0))},
            {('B', Point(x=4, y=1)), ('N', Point(x=4, y=0))},
            {('B', Point(x=0, y=2)), ('N', Point(x=4, y=0))},
            {('B', Point(x=1, y=2)), ('N', Point(x=4, y=0))},
            {('B', Point(x=4, y=2)), ('N', Point(x=4, y=0))},
            {('B', Point(x=0, y=3)), ('N', Point(x=4, y=0))},
            {('B', Point(x=2, y=3)), ('N', Point(x=4, y=0))},
            {('B', Point(x=3, y=3)), ('N', Point(x=4, y=0))},
            {('B', Point(x=4, y=3)), ('N', Point(x=4, y=0))},
            {('B', Point(x=1, y=4)), ('N', Point(x=4, y=0))},
            {('B', Point(x=2, y=4)), ('N', Point(x=4, y=0))},
            {('B', Point(x=3, y=4)), ('N', Point(x=4, y=0))},
            {('B', Point(x=4, y=4)), ('N', Point(x=4, y=0))},
            {('N', Point(x=0, y=1)), ('B', Point(x=0, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=0, y=1))},
            {('B', Point(x=4, y=0)), ('N', Point(x=0, y=1))},
            {('N', Point(x=0, y=1)), ('B', Point(x=1, y=1))},
            {('N', Point(x=0, y=1)), ('B', Point(x=2, y=1))},
            {('N', Point(x=0, y=1)), ('B', Point(x=3, y=1))},
            {('N', Point(x=0, y=1)), ('B', Point(x=4, y=1))},
            {('N', Point(x=0, y=1)), ('B', Point(x=0, y=2))},
            {('N', Point(x=0, y=1)), ('B', Point(x=3, y=2))},
            {('N', Point(x=0, y=1)), ('B', Point(x=4, y=2))},
            {('N', Point(x=0, y=1)), ('B', Point(x=0, y=3))},
            {('N', Point(x=0, y=1)), ('B', Point(x=3, y=3))},
            {('N', Point(x=0, y=1)), ('B', Point(x=4, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=0, y=1))},
            {('N', Point(x=0, y=1)), ('B', Point(x=1, y=4))},
            {('N', Point(x=0, y=1)), ('B', Point(x=2, y=4))},
            {('N', Point(x=0, y=1)), ('B', Point(x=4, y=4))},
            {('N', Point(x=1, y=1)), ('B', Point(x=1, y=0))},
            {('N', Point(x=1, y=1)), ('B', Point(x=4, y=0))},
            {('B', Point(x=0, y=1)), ('N', Point(x=1, y=1))},
            {('N', Point(x=1, y=1)), ('B', Point(x=2, y=1))},
            {('N', Point(x=1, y=1)), ('B', Point(x=3, y=1))},
            {('N', Point(x=1, y=1)), ('B', Point(x=4, y=1))},
            {('B', Point(x=1, y=2)), ('N', Point(x=1, y=1))},
            {('N', Point(x=1, y=1)), ('B', Point(x=4, y=2))},
            {('N', Point(x=1, y=1)), ('B', Point(x=1, y=3))},
            {('N', Point(x=1, y=1)), ('B', Point(x=4, y=3))},
            {('N', Point(x=1, y=1)), ('B', Point(x=0, y=4))},
            {('N', Point(x=1, y=1)), ('B', Point(x=1, y=4))},
            {('N', Point(x=1, y=1)), ('B', Point(x=2, y=4))},
            {('N', Point(x=1, y=1)), ('B', Point(x=3, y=4))},
            {('B', Point(x=2, y=0)), ('N', Point(x=2, y=1))},
            {('B', Point(x=0, y=1)), ('N', Point(x=2, y=1))},
            {('N', Point(x=2, y=1)), ('B', Point(x=1, y=1))},
            {('N', Point(x=2, y=1)), ('B', Point(x=3, y=1))},
            {('N', Point(x=2, y=1)), ('B', Point(x=4, y=1))},
            {('B', Point(x=2, y=2)), ('N', Point(x=2, y=1))},
            {('N', Point(x=2, y=1)), ('B', Point(x=2, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=2, y=1))},
            {('N', Point(x=2, y=1)), ('B', Point(x=1, y=4))},
            {('N', Point(x=2, y=1)), ('B', Point(x=2, y=4))},
            {('N', Point(x=2, y=1)), ('B', Point(x=3, y=4))},
            {('N', Point(x=2, y=1)), ('B', Point(x=4, y=4))},
            {('N', Point(x=3, y=1)), ('B', Point(x=0, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=3, y=1))},
            {('B', Point(x=0, y=1)), ('N', Point(x=3, y=1))},
            {('N', Point(x=3, y=1)), ('B', Point(x=1, y=1))},
            {('N', Point(x=3, y=1)), ('B', Point(x=2, y=1))},
            {('N', Point(x=3, y=1)), ('B', Point(x=4, y=1))},
            {('N', Point(x=3, y=1)), ('B', Point(x=0, y=2))},
            {('B', Point(x=3, y=2)), ('N', Point(x=3, y=1))},
            {('B', Point(x=0, y=3)), ('N', Point(x=3, y=1))},
            {('N', Point(x=3, y=1)), ('B', Point(x=3, y=3))},
            {('N', Point(x=3, y=1)), ('B', Point(x=1, y=4))},
            {('N', Point(x=3, y=1)), ('B', Point(x=2, y=4))},
            {('N', Point(x=3, y=1)), ('B', Point(x=3, y=4))},
            {('N', Point(x=3, y=1)), ('B', Point(x=4, y=4))},
            {('B', Point(x=0, y=0)), ('N', Point(x=4, y=1))},
            {('B', Point(x=1, y=0)), ('N', Point(x=4, y=1))},
            {('B', Point(x=4, y=0)), ('N', Point(x=4, y=1))},
            {('B', Point(x=0, y=1)), ('N', Point(x=4, y=1))},
            {('N', Point(x=4, y=1)), ('B', Point(x=1, y=1))},
            {('N', Point(x=4, y=1)), ('B', Point(x=2, y=1))},
            {('B', Point(x=3, y=1)), ('N', Point(x=4, y=1))},
            {('N', Point(x=4, y=1)), ('B', Point(x=0, y=2))},
            {('B', Point(x=1, y=2)), ('N', Point(x=4, y=1))},
            {('B', Point(x=4, y=2)), ('N', Point(x=4, y=1))},
            {('B', Point(x=0, y=3)), ('N', Point(x=4, y=1))},
            {('B', Point(x=1, y=3)), ('N', Point(x=4, y=1))},
            {('N', Point(x=4, y=1)), ('B', Point(x=4, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=4, y=1))},
            {('N', Point(x=4, y=1)), ('B', Point(x=2, y=4))},
            {('B', Point(x=3, y=4)), ('N', Point(x=4, y=1))},
            {('B', Point(x=4, y=4)), ('N', Point(x=4, y=1))},
            {('N', Point(x=0, y=2)), ('B', Point(x=0, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=0, y=2))},
            {('N', Point(x=0, y=2)), ('B', Point(x=4, y=0))},
            {('B', Point(x=0, y=1)), ('N', Point(x=0, y=2))},
            {('N', Point(x=0, y=2)), ('B', Point(x=3, y=1))},
            {('N', Point(x=0, y=2)), ('B', Point(x=4, y=1))},
            {('B', Point(x=1, y=2)), ('N', Point(x=0, y=2))},
            {('N', Point(x=0, y=2)), ('B', Point(x=2, y=2))},
            {('N', Point(x=0, y=2)), ('B', Point(x=3, y=2))},
            {('N', Point(x=0, y=2)), ('B', Point(x=4, y=2))},
            {('N', Point(x=0, y=2)), ('B', Point(x=0, y=3))},
            {('N', Point(x=0, y=2)), ('B', Point(x=3, y=3))},
            {('N', Point(x=0, y=2)), ('B', Point(x=4, y=3))},
            {('N', Point(x=0, y=2)), ('B', Point(x=0, y=4))},
            {('N', Point(x=0, y=2)), ('B', Point(x=3, y=4))},
            {('N', Point(x=0, y=2)), ('B', Point(x=4, y=4))},
            {('N', Point(x=1, y=2)), ('B', Point(x=1, y=0))},
            {('B', Point(x=4, y=0)), ('N', Point(x=1, y=2))},
            {('N', Point(x=1, y=2)), ('B', Point(x=1, y=1))},
            {('N', Point(x=1, y=2)), ('B', Point(x=4, y=1))},
            {('N', Point(x=1, y=2)), ('B', Point(x=0, y=2))},
            {('B', Point(x=2, y=2)), ('N', Point(x=1, y=2))},
            {('N', Point(x=1, y=2)), ('B', Point(x=3, y=2))},
            {('N', Point(x=1, y=2)), ('B', Point(x=4, y=2))},
            {('N', Point(x=1, y=2)), ('B', Point(x=1, y=3))},
            {('N', Point(x=1, y=2)), ('B', Point(x=4, y=3))},
            {('N', Point(x=1, y=2)), ('B', Point(x=1, y=4))},
            {('N', Point(x=1, y=2)), ('B', Point(x=4, y=4))},
            {('B', Point(x=2, y=0)), ('N', Point(x=2, y=2))},
            {('B', Point(x=2, y=1)), ('N', Point(x=2, y=2))},
            {('B', Point(x=0, y=2)), ('N', Point(x=2, y=2))},
            {('B', Point(x=1, y=2)), ('N', Point(x=2, y=2))},
            {('B', Point(x=3, y=2)), ('N', Point(x=2, y=2))},
            {('B', Point(x=4, y=2)), ('N', Point(x=2, y=2))},
            {('B', Point(x=2, y=3)), ('N', Point(x=2, y=2))},
            {('B', Point(x=2, y=4)), ('N', Point(x=2, y=2))},
            {('N', Point(x=3, y=2)), ('B', Point(x=0, y=0))},
            {('N', Point(x=3, y=2)), ('B', Point(x=3, y=0))},
            {('N', Point(x=3, y=2)), ('B', Point(x=0, y=1))},
            {('N', Point(x=3, y=2)), ('B', Point(x=3, y=1))},
            {('N', Point(x=3, y=2)), ('B', Point(x=0, y=2))},
            {('N', Point(x=3, y=2)), ('B', Point(x=1, y=2))},
            {('N', Point(x=3, y=2)), ('B', Point(x=2, y=2))},
            {('N', Point(x=3, y=2)), ('B', Point(x=4, y=2))},
            {('N', Point(x=3, y=2)), ('B', Point(x=0, y=3))},
            {('N', Point(x=3, y=2)), ('B', Point(x=3, y=3))},
            {('N', Point(x=3, y=2)), ('B', Point(x=0, y=4))},
            {('N', Point(x=3, y=2)), ('B', Point(x=3, y=4))},
            {('N', Point(x=4, y=2)), ('B', Point(x=0, y=0))},
            {('B', Point(x=1, y=0)), ('N', Point(x=4, y=2))},
            {('B', Point(x=4, y=0)), ('N', Point(x=4, y=2))},
            {('B', Point(x=0, y=1)), ('N', Point(x=4, y=2))},
            {('N', Point(x=4, y=2)), ('B', Point(x=1, y=1))},
            {('B', Point(x=4, y=1)), ('N', Point(x=4, y=2))},
            {('N', Point(x=4, y=2)), ('B', Point(x=0, y=2))},
            {('B', Point(x=1, y=2)), ('N', Point(x=4, y=2))},
            {('B', Point(x=2, y=2)), ('N', Point(x=4, y=2))},
            {('B', Point(x=3, y=2)), ('N', Point(x=4, y=2))},
            {('B', Point(x=0, y=3)), ('N', Point(x=4, y=2))},
            {('N', Point(x=4, y=2)), ('B', Point(x=1, y=3))},
            {('N', Point(x=4, y=2)), ('B', Point(x=4, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=4, y=2))},
            {('N', Point(x=4, y=2)), ('B', Point(x=1, y=4))},
            {('N', Point(x=4, y=2)), ('B', Point(x=4, y=4))},
            {('N', Point(x=0, y=3)), ('B', Point(x=0, y=0))},
            {('N', Point(x=0, y=3)), ('B', Point(x=1, y=0))},
            {('N', Point(x=0, y=3)), ('B', Point(x=2, y=0))},
            {('N', Point(x=0, y=3)), ('B', Point(x=4, y=0))},
            {('N', Point(x=0, y=3)), ('B', Point(x=0, y=1))},
            {('N', Point(x=0, y=3)), ('B', Point(x=3, y=1))},
            {('N', Point(x=0, y=3)), ('B', Point(x=4, y=1))},
            {('N', Point(x=0, y=3)), ('B', Point(x=0, y=2))},
            {('N', Point(x=0, y=3)), ('B', Point(x=3, y=2))},
            {('N', Point(x=0, y=3)), ('B', Point(x=4, y=2))},
            {('N', Point(x=0, y=3)), ('B', Point(x=1, y=3))},
            {('N', Point(x=0, y=3)), ('B', Point(x=2, y=3))},
            {('N', Point(x=0, y=3)), ('B', Point(x=3, y=3))},
            {('N', Point(x=0, y=3)), ('B', Point(x=4, y=3))},
            {('N', Point(x=0, y=3)), ('B', Point(x=0, y=4))},
            {('N', Point(x=0, y=3)), ('B', Point(x=3, y=4))},
            {('N', Point(x=0, y=3)), ('B', Point(x=4, y=4))},
            {('N', Point(x=1, y=3)), ('B', Point(x=0, y=0))},
            {('B', Point(x=1, y=0)), ('N', Point(x=1, y=3))},
            {('N', Point(x=1, y=3)), ('B', Point(x=2, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=1, y=3))},
            {('N', Point(x=1, y=3)), ('B', Point(x=1, y=1))},
            {('N', Point(x=1, y=3)), ('B', Point(x=4, y=1))},
            {('B', Point(x=1, y=2)), ('N', Point(x=1, y=3))},
            {('N', Point(x=1, y=3)), ('B', Point(x=4, y=2))},
            {('B', Point(x=0, y=3)), ('N', Point(x=1, y=3))},
            {('N', Point(x=1, y=3)), ('B', Point(x=2, y=3))},
            {('N', Point(x=1, y=3)), ('B', Point(x=3, y=3))},
            {('N', Point(x=1, y=3)), ('B', Point(x=4, y=3))},
            {('N', Point(x=1, y=3)), ('B', Point(x=1, y=4))},
            {('N', Point(x=1, y=3)), ('B', Point(x=4, y=4))},
            {('B', Point(x=0, y=0)), ('N', Point(x=2, y=3))},
            {('B', Point(x=1, y=0)), ('N', Point(x=2, y=3))},
            {('B', Point(x=2, y=0)), ('N', Point(x=2, y=3))},
            {('B', Point(x=3, y=0)), ('N', Point(x=2, y=3))},
            {('B', Point(x=4, y=0)), ('N', Point(x=2, y=3))},
            {('N', Point(x=2, y=3)), ('B', Point(x=2, y=1))},
            {('B', Point(x=2, y=2)), ('N', Point(x=2, y=3))},
            {('B', Point(x=0, y=3)), ('N', Point(x=2, y=3))},
            {('B', Point(x=1, y=3)), ('N', Point(x=2, y=3))},
            {('N', Point(x=2, y=3)), ('B', Point(x=3, y=3))},
            {('N', Point(x=2, y=3)), ('B', Point(x=4, y=3))},
            {('N', Point(x=2, y=3)), ('B', Point(x=2, y=4))},
            {('N', Point(x=3, y=3)), ('B', Point(x=1, y=0))},
            {('N', Point(x=3, y=3)), ('B', Point(x=2, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=3, y=3))},
            {('N', Point(x=3, y=3)), ('B', Point(x=4, y=0))},
            {('B', Point(x=0, y=1)), ('N', Point(x=3, y=3))},
            {('N', Point(x=3, y=3)), ('B', Point(x=3, y=1))},
            {('N', Point(x=3, y=3)), ('B', Point(x=0, y=2))},
            {('N', Point(x=3, y=3)), ('B', Point(x=3, y=2))},
            {('N', Point(x=3, y=3)), ('B', Point(x=0, y=3))},
            {('N', Point(x=3, y=3)), ('B', Point(x=1, y=3))},
            {('N', Point(x=3, y=3)), ('B', Point(x=2, y=3))},
            {('N', Point(x=3, y=3)), ('B', Point(x=4, y=3))},
            {('N', Point(x=3, y=3)), ('B', Point(x=0, y=4))},
            {('N', Point(x=3, y=3)), ('B', Point(x=3, y=4))},
            {('N', Point(x=4, y=3)), ('B', Point(x=0, y=0))},
            {('B', Point(x=2, y=0)), ('N', Point(x=4, y=3))},
            {('B', Point(x=3, y=0)), ('N', Point(x=4, y=3))},
            {('B', Point(x=4, y=0)), ('N', Point(x=4, y=3))},
            {('B', Point(x=0, y=1)), ('N', Point(x=4, y=3))},
            {('N', Point(x=4, y=3)), ('B', Point(x=1, y=1))},
            {('N', Point(x=4, y=3)), ('B', Point(x=4, y=1))},
            {('N', Point(x=4, y=3)), ('B', Point(x=0, y=2))},
            {('B', Point(x=1, y=2)), ('N', Point(x=4, y=3))},
            {('B', Point(x=4, y=2)), ('N', Point(x=4, y=3))},
            {('B', Point(x=0, y=3)), ('N', Point(x=4, y=3))},
            {('N', Point(x=4, y=3)), ('B', Point(x=1, y=3))},
            {('N', Point(x=4, y=3)), ('B', Point(x=2, y=3))},
            {('N', Point(x=4, y=3)), ('B', Point(x=3, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=4, y=3))},
            {('N', Point(x=4, y=3)), ('B', Point(x=1, y=4))},
            {('N', Point(x=4, y=3)), ('B', Point(x=4, y=4))},
            {('B', Point(x=0, y=0)), ('N', Point(x=0, y=4))},
            {('B', Point(x=1, y=0)), ('N', Point(x=0, y=4))},
            {('B', Point(x=2, y=0)), ('N', Point(x=0, y=4))},
            {('B', Point(x=3, y=0)), ('N', Point(x=0, y=4))},
            {('B', Point(x=0, y=1)), ('N', Point(x=0, y=4))},
            {('B', Point(x=1, y=1)), ('N', Point(x=0, y=4))},
            {('B', Point(x=2, y=1)), ('N', Point(x=0, y=4))},
            {('B', Point(x=4, y=1)), ('N', Point(x=0, y=4))},
            {('B', Point(x=0, y=2)), ('N', Point(x=0, y=4))},
            {('B', Point(x=3, y=2)), ('N', Point(x=0, y=4))},
            {('B', Point(x=4, y=2)), ('N', Point(x=0, y=4))},
            {('B', Point(x=0, y=3)), ('N', Point(x=0, y=4))},
            {('B', Point(x=3, y=3)), ('N', Point(x=0, y=4))},
            {('B', Point(x=4, y=3)), ('N', Point(x=0, y=4))},
            {('B', Point(x=1, y=4)), ('N', Point(x=0, y=4))},
            {('B', Point(x=2, y=4)), ('N', Point(x=0, y=4))},
            {('B', Point(x=3, y=4)), ('N', Point(x=0, y=4))},
            {('B', Point(x=4, y=4)), ('N', Point(x=0, y=4))},
            {('N', Point(x=1, y=4)), ('B', Point(x=0, y=0))},
            {('B', Point(x=1, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=2, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=3, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=4, y=0)), ('N', Point(x=1, y=4))},
            {('B', Point(x=0, y=1)), ('N', Point(x=1, y=4))},
            {('N', Point(x=1, y=4)), ('B', Point(x=1, y=1))},
            {('N', Point(x=1, y=4)), ('B', Point(x=2, y=1))},
            {('N', Point(x=1, y=4)), ('B', Point(x=3, y=1))},
            {('B', Point(x=1, y=2)), ('N', Point(x=1, y=4))},
            {('B', Point(x=4, y=2)), ('N', Point(x=1, y=4))},
            {('N', Point(x=1, y=4)), ('B', Point(x=1, y=3))},
            {('N', Point(x=1, y=4)), ('B', Point(x=4, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=1, y=4))},
            {('N', Point(x=1, y=4)), ('B', Point(x=2, y=4))},
            {('N', Point(x=1, y=4)), ('B', Point(x=3, y=4))},
            {('N', Point(x=1, y=4)), ('B', Point(x=4, y=4))},
            {('N', Point(x=2, y=4)), ('B', Point(x=0, y=0))},
            {('N', Point(x=2, y=4)), ('B', Point(x=1, y=0))},
            {('N', Point(x=2, y=4)), ('B', Point(x=2, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=2, y=4))},
            {('N', Point(x=2, y=4)), ('B', Point(x=4, y=0))},
            {('B', Point(x=0, y=1)), ('N', Point(x=2, y=4))},
            {('N', Point(x=2, y=4)), ('B', Point(x=1, y=1))},
            {('N', Point(x=2, y=4)), ('B', Point(x=2, y=1))},
            {('N', Point(x=2, y=4)), ('B', Point(x=3, y=1))},
            {('N', Point(x=2, y=4)), ('B', Point(x=4, y=1))},
            {('N', Point(x=2, y=4)), ('B', Point(x=2, y=2))},
            {('N', Point(x=2, y=4)), ('B', Point(x=2, y=3))},
            {('N', Point(x=2, y=4)), ('B', Point(x=0, y=4))},
            {('N', Point(x=2, y=4)), ('B', Point(x=1, y=4))},
            {('N', Point(x=2, y=4)), ('B', Point(x=3, y=4))},
            {('N', Point(x=2, y=4)), ('B', Point(x=4, y=4))},
            {('B', Point(x=0, y=0)), ('N', Point(x=3, y=4))},
            {('B', Point(x=1, y=0)), ('N', Point(x=3, y=4))},
            {('B', Point(x=2, y=0)), ('N', Point(x=3, y=4))},
            {('B', Point(x=3, y=0)), ('N', Point(x=3, y=4))},
            {('B', Point(x=4, y=0)), ('N', Point(x=3, y=4))},
            {('N', Point(x=3, y=4)), ('B', Point(x=1, y=1))},
            {('N', Point(x=3, y=4)), ('B', Point(x=2, y=1))},
            {('B', Point(x=3, y=1)), ('N', Point(x=3, y=4))},
            {('B', Point(x=4, y=1)), ('N', Point(x=3, y=4))},
            {('N', Point(x=3, y=4)), ('B', Point(x=0, y=2))},
            {('B', Point(x=3, y=2)), ('N', Point(x=3, y=4))},
            {('B', Point(x=0, y=3)), ('N', Point(x=3, y=4))},
            {('N', Point(x=3, y=4)), ('B', Point(x=3, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=3, y=4))},
            {('N', Point(x=3, y=4)), ('B', Point(x=1, y=4))},
            {('N', Point(x=3, y=4)), ('B', Point(x=2, y=4))},
            {('B', Point(x=4, y=4)), ('N', Point(x=3, y=4))},
            {('B', Point(x=1, y=0)), ('N', Point(x=4, y=4))},
            {('N', Point(x=4, y=4)), ('B', Point(x=2, y=0))},
            {('B', Point(x=3, y=0)), ('N', Point(x=4, y=4))},
            {('B', Point(x=4, y=0)), ('N', Point(x=4, y=4))},
            {('B', Point(x=0, y=1)), ('N', Point(x=4, y=4))},
            {('N', Point(x=4, y=4)), ('B', Point(x=2, y=1))},
            {('N', Point(x=4, y=4)), ('B', Point(x=3, y=1))},
            {('N', Point(x=4, y=4)), ('B', Point(x=4, y=1))},
            {('N', Point(x=4, y=4)), ('B', Point(x=0, y=2))},
            {('B', Point(x=1, y=2)), ('N', Point(x=4, y=4))},
            {('N', Point(x=4, y=4)), ('B', Point(x=4, y=2))},
            {('B', Point(x=0, y=3)), ('N', Point(x=4, y=4))},
            {('N', Point(x=4, y=4)), ('B', Point(x=1, y=3))},
            {('N', Point(x=4, y=4)), ('B', Point(x=4, y=3))},
            {('B', Point(x=0, y=4)), ('N', Point(x=4, y=4))},
            {('N', Point(x=4, y=4)), ('B', Point(x=1, y=4))},
            {('N', Point(x=4, y=4)), ('B', Point(x=2, y=4))},
            {('N', Point(x=4, y=4)), ('B', Point(x=3, y=4))}
        ]

        self.assertEqual(len(result), len(expected))

        for item in expected:
            with self.subTest(item=item):
                self.assertIn(item, result)


class PointCase(unittest.TestCase):

    def test_flip_diagonal_left(self):
        board_size = BoardSize(6, 6)
        self.assertEqual(Point(3, 0).flipped(board_size, DIAGONAL_LEFT), Point(0, 3))

    def test_flip_diagonal_right(self):
        board_size = BoardSize(6, 6)
        self.assertEqual(Point(3, 0).flipped(board_size, DIAGONAL_RIGHT), Point(5, 2))

    def test_flip_vertical(self):
        board_size = BoardSize(6, 6)
        self.assertEqual(Point(2, 1).flipped(board_size, VERTICAL), Point(2, 4))

    def test_flip_horizontal(self):
        board_size = BoardSize(6, 6)
        self.assertEqual(Point(2, 1).flipped(board_size, HORIZONTAL), Point(3, 1))


class LinkedListNodeCase(unittest.TestCase):

    def setUp(self):
        self.num_nodes = 10
        self.head = LinkedListNode(0)
        current_node = self.head
        for i in range(1, self.num_nodes):
            current_node.next_node = LinkedListNode(i, previous_node=current_node)
            current_node = current_node.next_node
        self.tail = current_node

    def tearDown(self):
        current_node = self.head
        while True:
            next_node = current_node.next_node
            current_node.next_node = current_node.previous_node = None

            if next_node is None:
                break

            current_node = next_node

    def test_traverse_forward(self):
        for position, node in enumerate(self.head.traverse_forward()):
            self.assertEqual(node.value, position)

        self.assertEqual(position, self.num_nodes - 1)

    def test_traverse_backwards(self):
        for node in self.head.traverse_forward():
            pass

        self.assertEqual(
            [n.value for n in node.traverse_backwards()],
            list(range(self.num_nodes - 1, -1, -1)))

    def test_remove_myself_when_head(self):
        next_head = self.head.next_node
        self.head.remove_myself()
        self.assertIs(next_head.previous_node, None)

        self.assertEqual(
            [n.value for n in next_head.traverse_forward()],
            list(range(1, self.num_nodes)))

        self.assertEqual(
            [n.value for n in next_head.traverse_backwards()],
            [1])

    def test_remove_myself_when_tail(self):
        next_tail = self.tail.previous_node
        self.tail.remove_myself()
        self.assertIs(next_tail.next_node, None)

        self.assertEqual(
            [n.value for n in next_tail.traverse_backwards()],
            list(range(self.num_nodes - 2, -1, -1)))

        self.assertEqual(
            [n.value for n in next_tail.traverse_forward()],
            [self.num_nodes - 2])

    def test_remove_myself_in_middle(self):
        second_node = self.head.next_node
        third_node = second_node.next_node
        second_node.remove_myself()
        self.assertIs(self.head.next_node, third_node)
        self.assertIs(third_node.previous_node, self.head)

        expected = list(range(self.num_nodes))
        expected.remove(1)

        self.assertEqual(
            [n.value for n in self.tail.traverse_backwards()],
            list(reversed(expected)))

        self.assertEqual(
            [n.value for n in self.head.traverse_forward()],
            expected)

    def test_reinsert(self):
        second_node = self.head.next_node
        third_node = second_node.next_node
        second_node.remove_myself()
        second_node.reinsert()
        self.assertIs(self.head.next_node, second_node)
        self.assertIs(third_node.previous_node, second_node)

        expected = list(range(self.num_nodes))

        self.assertEqual(
            [n.value for n in self.head.traverse_forward()],
            expected)

        self.assertEqual(
            [n.value for n in self.tail.traverse_backwards()],
            list(reversed(expected)))


class SpreadInLineCase(unittest.TestCase):

    def test_out_of_bounds(self):
        with self.assertRaises(ValueError):
            spread_in_line(BoardSize(1, 1), Point(2, 2), NORTH)

    def test_spread_nort(self):
        points = list(spread_in_line(BoardSize(3, 4), Point(1, 2), NORTH))
        self.assertEqual(len(points), 2)
        self.assertEqual(set(points), {Point(1, 0), Point(1, 1)})

    def test_spread_nort_east(self):
        points = list(spread_in_line(BoardSize(5, 4), Point(0, 3), NORTH_EAST))
        self.assertEqual(len(points), 3)
        self.assertEqual(set(points), {Point(1, 2), Point(2, 1), Point(3, 0)})

    def test_spread_east(self):
        points = list(spread_in_line(BoardSize(7, 5), Point(2, 3), EAST))
        self.assertEqual(len(points), 4)
        self.assertEqual(
            set(points),
            {Point(3, 3), Point(4, 3), Point(5, 3), Point(6, 3)})

    def test_spread_south_east(self):
        points = list(spread_in_line(BoardSize(7, 3), Point(1, 0), SOUTH_EAST))
        self.assertEqual(len(points), 2)
        self.assertEqual(set(points), {Point(2, 1), Point(3, 2)})

    def test_spread_south(self):
        points = list(spread_in_line(BoardSize(3, 3), Point(0, 0), SOUTH))
        self.assertEqual(len(points), 2)
        self.assertEqual(set(points), {Point(0, 1), Point(0, 2)})

    def test_spread_south_west(self):
        points = list(spread_in_line(BoardSize(4, 7), Point(3, 1), SOUTH_WEST))
        self.assertEqual(len(points), 3)
        self.assertEqual(set(points), {Point(2, 2), Point(1, 3), Point(0, 4)})

    def test_spread_west(self):
        points = list(spread_in_line(BoardSize(3, 3), Point(1, 2), WEST))
        self.assertEqual(len(points), 1)
        self.assertEqual(set(points), {Point(0, 2)})

    def test_spread_north_west(self):
        points = list(spread_in_line(BoardSize(5, 7), Point(4, 6), NORTH_WEST))
        self.assertEqual(len(points), 4)
        self.assertEqual(
            set(points),
            {Point(3, 5), Point(2, 4), Point(1, 3), Point(0, 2)})


class ChessPieceSpreadCase(unittest.TestCase):

    def test_king(self):
        points = list(PIECE_SPREAD['K'](BoardSize(4, 4), Point(2, 2)))
        self.assertEqual(len(points), 8)
        self.assertEqual(
            set(points),
            {Point(1, 2), Point(1, 1), Point(2, 1), Point(3, 1), Point(3, 2),
             Point(3, 3), Point(2, 3), Point(1, 3)})

    def test_queen(self):
        points = list(PIECE_SPREAD['Q'](BoardSize(4, 4), Point(2, 2)))
        self.assertEqual(len(points), 11)
        self.assertEqual(
            set(points),
            {Point(1, 2), Point(0, 2), Point(1, 1), Point(0, 0), Point(2, 1),
             Point(2, 0), Point(3, 1), Point(3, 2), Point(3, 3), Point(2, 3),
             Point(1, 3)})

    def test_rook(self):
        points = list(PIECE_SPREAD['R'](BoardSize(4, 4), Point(2, 2)))
        self.assertEqual(len(points), 6)
        self.assertEqual(
            set(points),
            {Point(1, 2), Point(0, 2), Point(2, 1), Point(2, 0), Point(3, 2),
             Point(2, 3)})

    def test_bishop(self):
        points = list(PIECE_SPREAD['B'](BoardSize(4, 4), Point(2, 2)))
        self.assertEqual(len(points), 5)
        self.assertEqual(
            set(points),
            {Point(1, 1), Point(0, 0), Point(3, 1), Point(3, 3), Point(1, 3)})

    def test_knight(self):
        points = list(PIECE_SPREAD['N'](BoardSize(7, 4), Point(3, 1)))
        self.assertEqual(len(points), 6)
        self.assertEqual(
            set(points),
            {Point(1, 0), Point(5, 0), Point(5, 2), Point(4, 3), Point(2, 3),
             Point(1, 2)})


class BoardCase(unittest.TestCase):

    def test_board(self):
        board_size = BoardSize(5, 3)
        board = Board(board_size)

        points = [Point(x, y) for y, x in itertools.product(range(board_size.height), range(board_size.width))]

        for point in points:
            with self.subTest(item=point):
                self.assertEqual(board.taken_grid[point.y][point.x], FREE)
                self.assertEqual(board.node_grid[point.y][point.x].value, point)

        node_count = 0
        for node in board.linked_positions_head.traverse_forward():
            node_count += 1
            with self.subTest(item=node):
                if node.previous_node is None:
                    self.assertEqual(node.value, Point(0, 0))
                else:
                    point = points[points.index(node.value) - 1]
                    self.assertIs(node.previous_node, board.node_grid[point.y][point.x])

                if node.next_node is None:
                    self.assertEqual(node.value, Point(board_size.width - 1, board_size.height - 1))
                else:
                    point = points[points.index(node.value) + 1]
                    self.assertIs(node.next_node, board.node_grid[point.y][point.x])

        self.assertEqual(node_count, board_size.width * board_size.height)


def stress_test():
    PIECES = ('K', 'Q', 'R', 'B', 'N')

    progress = itertools.cycle('|/-\\')

    while True:
        size = randint(2, 5)
        total_pieces = randint(1, 10)
        piece_count = Counter(choice(PIECES) for i in range(total_pieces))

        solver = ChessProblemSolver(BoardSize(size, size), piece_count)
        result1 = list(solver.solve())
        result2 = list(solver.solve(disable_optimizations=True))

        if len(result1) != len(result2) or [i for i in result1 if i not in result2]:
            print('error: {0}x{0}'.format(size), piece_count)

        sys.stdout.write(next(progress))
        sys.stdout.write('\b')
        sys.stdout.flush()


if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == 'stress-test':
        stress_test()
    else:
        unittest.main()
