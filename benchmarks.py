import itertools
from time import time

from chess_problem import *


def benchmark_piece_priorities():
    pieces = ('Q', 'R', 'B', 'K', 'N')

    possible_priorities = list(itertools.permutations(pieces, len(pieces)))

    board_size = BoardSize(5, 5)
    piece_count = {'K': 1, 'Q': 1, 'B': 1, 'R': 1, 'N': 1}

    # taken from benchmarks of all possible
    best_possible_priorities = [
        ('Q', 'R', 'B', 'K', 'N'),
        ('Q', 'R', 'B', 'N', 'K'),
        ('Q', 'R', 'K', 'B', 'N'),
        ('Q', 'R', 'K', 'N', 'B'),
        ('R', 'Q', 'K', 'N', 'B'),
        ('Q', 'K', 'R', 'N', 'B'),
        ('K', 'Q', 'R', 'N', 'B'),
        ('R', 'Q', 'N', 'K', 'B'),
        ('Q', 'R', 'N', 'K', 'B'),
        ('Q', 'N', 'R', 'K', 'B'),
        ('N', 'Q', 'R', 'K', 'B'),
        ('Q', 'N', 'K', 'R', 'B'),
        ('K', 'Q', 'N', 'R', 'B'),
        ('Q', 'K', 'N', 'R', 'B'),
        ('N', 'Q', 'K', 'R', 'B'),
        ('K', 'Q', 'R', 'B', 'N'),
        ('R', 'Q', 'K', 'B', 'N'),
        ('Q', 'K', 'R', 'B', 'N'),
        ('R', 'K', 'Q', 'N', 'B'),
        ('K', 'R', 'Q', 'N', 'B'),
        ('R', 'Q', 'B', 'K', 'N'),
    ]

    results = []

    solver = ChessProblemSolver(board_size, piece_count)
    for priority in reversed(best_possible_priorities):
        solver.pieces_by_priority = priority

        timings = []
        for i in range(4):
            start = time()
            solver.solve()
            timings.append(time() - start)

        mean = sum(timings) / len(timings)
        results.append((priority, mean))
        print(priority, mean)

    for result in sorted(results, key=lambda item: item[1]):
        print(result)


if __name__ == '__main__':
    benchmark_piece_priorities()
