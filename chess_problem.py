import itertools
import logging
import sqlite3
from collections import namedtuple, defaultdict
from math import ceil
from tempfile import mkstemp
from time import time
from os import unlink

log = logging.getLogger(__name__)

BasePoint = namedtuple('BasePoint', 'x y')


DIAGONAL_LEFT = 'dl'
DIAGONAL_RIGHT = 'dr'
VERTICAL = 'v'
HORIZONTAL = 'h'


class Point(BasePoint):

    def flipped(self, board_size, direction):
        if direction == DIAGONAL_LEFT:
            new_x = self.y
            new_y = self.x
        elif direction == DIAGONAL_RIGHT:
            new_x = board_size.height - self.y - 1
            new_y = board_size.width - self.x - 1
        elif direction == VERTICAL:
            new_x = self.x
            new_y = board_size.height - self.y - 1
        elif direction == HORIZONTAL:
            new_x = board_size.width - self.x - 1
            new_y = self.y
        else:
            raise ValueError('invalid direction')

        if new_x < 0 or new_x > board_size.width or new_y < 0 or new_y > board_size.height:
            return None

        return Point(new_x, new_y)


BoardSize = namedtuple('BoardSize', 'width height')

NORTH = 'n'
NORTH_EAST = 'ne'
EAST = 'e'
SOUTH_EAST = 'se'
SOUTH = 's'
SOUTH_WEST = 'sw'
WEST = 'w'
NORTH_WEST = 'nw'


class LinkedListNode(object):
    """A node from a doubly linked list."""

    def __init__(self, value, next_node=None, previous_node=None):
        self.value = value
        self.next_node = next_node
        self.previous_node = previous_node

    def __str__(self):
        return 'LinkedListNode{{value={v}, have_next={hn}, next_node_value={nv}, have_previous={hp}, previous_node_value={pv}}}'.format(
            v=self.value,
            hn=self.next_node is not None,
            nv=self.next_node.value if self.next_node is not None else None,
            hp=self.previous_node is not None,
            pv=self.previous_node.value if self.previous_node is not None else None
        )

    def remove_myself(self):
        """Remove myself from the linked list by connecting my previous and next nodes."""
        if self.previous_node is None and self.next_node is None:
            return

        if self.previous_node is None:
            self.next_node.previous_node = None
        else:
            self.previous_node.next_node = self.next_node

        if self.next_node is None:
            self.previous_node.next_node = None
        else:
            self.next_node.previous_node = self.previous_node

    def reinsert(self):
        """Reinsert myself in the linked list at my previous position."""
        if self.previous_node is None and self.next_node is None:
            return

        if self.previous_node is not None:
            self.previous_node.next_node = self

        if self.next_node is not None:
            self.next_node.previous_node = self

    def traverse_forward(self):
        current_node = self
        while current_node is not None:
            yield current_node
            current_node = current_node.next_node

    def traverse_backwards(self):
        current_node = self
        while current_node is not None:
            yield current_node
            current_node = current_node.previous_node


def spread_in_line(board_size, starting_position, direction):
    """
    Return the positions retrieved by walking a 2-dimensional board starting
    from a given position towards a cardinal direction.

    Arguments:
    - board_size -- an object containing 2 integer properties named width and height
      specifying the size of the board
    - starting_position -- an object containing 2 integer properties named x and y
      specifying the starting position on the board

    Returns: a list of Point(x, y) objects
    """
    if (starting_position.y < 0 or starting_position.y > board_size.height - 1 or
            starting_position.x < 0 or starting_position.x > board_size.width - 1):
        raise ValueError('position out of board bounds')

    if direction == NORTH:
        return (Point(starting_position.x, i) for i in range(starting_position.y - 1, -1, -1))
    elif direction == NORTH_EAST:
        steps = min(board_size.width - 1 - starting_position.x, starting_position.y)
        return (Point(starting_position.x + i, starting_position.y - i) for i in range(1, steps + 1))
    elif direction == EAST:
        return (Point(i, starting_position.y) for i in range(starting_position.x + 1, board_size.width))
    elif direction == SOUTH_EAST:
        steps = min(board_size.width - 1 - starting_position.x, board_size.height - 1 - starting_position.y)
        return (Point(starting_position.x + i, starting_position.y + i) for i in range(1, steps + 1))
    elif direction == SOUTH:
        return (Point(starting_position.x, i) for i in range(starting_position.y + 1, board_size.height))
    elif direction == SOUTH_WEST:
        steps = min(starting_position.x, board_size.height - (starting_position.y + 1))
        return (Point(starting_position.x - i, starting_position.y + i) for i in range(1, steps + 1))
    elif direction == WEST:
        return (Point(i, starting_position.y) for i in range(starting_position.x - 1, -1, -1))
    elif direction == NORTH_WEST:
        steps = min(starting_position.x, starting_position.y)
        return (Point(starting_position.x - i, starting_position.y - i) for i in range(1, steps + 1))
    else:
        raise ValueError('invalid direction')


def knight_spread(board_size, position):
    return (i for i in (Point(position.x - 2, position.y - 1), Point(position.x - 1, position.y - 2),
                        Point(position.x + 1, position.y - 2), Point(position.x + 2, position.y - 1),
                        Point(position.x + 2, position.y + 1), Point(position.x + 1, position.y + 2),
                        Point(position.x - 1, position.y + 2), Point(position.x - 2, position.y + 1))
            if i.x >= 0 and i.y >= 0 and i.x < board_size.width and i.y < board_size.height)


PIECE_SPREAD = {
    'K': lambda board_size, position: itertools.chain.from_iterable(
        itertools.islice(spread_in_line(board_size, position, d), 1)
        for d in (WEST, NORTH_WEST, NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST)),
    'Q': lambda board_size, position: itertools.chain.from_iterable(
        spread_in_line(board_size, position, d)
        for d in (WEST, NORTH_WEST, NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST)),
    'R': lambda board_size, position: itertools.chain.from_iterable(
        spread_in_line(board_size, position, d)
        for d in (WEST, NORTH, EAST, SOUTH)),
    'B': lambda board_size, position: itertools.chain.from_iterable(
        spread_in_line(board_size, position, d)
        for d in (NORTH_WEST, NORTH_EAST, SOUTH_EAST, SOUTH_WEST)),
    'N': knight_spread
}


FREE = 0
THREATENED = 1
OCCUPIED = 2


class Board(object):
    """
    An object representing a chess board.

    Attributes:
    - board_size -- an object containing 2 integer properties named width and height
      specifying the size of the board
    - linked_positions_head -- the head node of a doubly linked list linking all the board
      positions sequentially
    - node_grid -- a 2d list mapping the linked_positions nodes to the chess
      board (x, y) positions
    - taken_grid -- a 2d list storing the current occupation status of each board
      positions
    """

    def __init__(self, board_size):
        if board_size.width <= 0 or board_size.height <= 0:
            raise ValueError('invalid board size')

        self.board_size = board_size

        self.linked_positions_head = None
        self.node_grid = []
        self.taken_grid = []

        previous_node = None
        for y in range(board_size.height):
            node_row = []
            taken_row = []

            for x in range(board_size.width):
                taken_row.append(FREE)

                new_node = LinkedListNode(Point(x, y), previous_node=previous_node)
                node_row.append(new_node)

                if previous_node is None:
                    self.linked_positions_head = new_node
                else:
                    previous_node.next_node = new_node

                previous_node = new_node

            self.node_grid.append(node_row)
            self.taken_grid.append(taken_row)


class LogWrapper(object):
    """Wrap an object so that it's only executed when its string representation
    is required."""

    def __init__(self, object_):
        self.object = object_

    def __str__(self):
        res = self.object
        if callable(res):
            res = res()

        if res is None:
            return ''

        if isinstance(res, str):
            return res

        if hasattr(res, '__iter__'):
            return str([i for i in res])

        return str(res)


def draw_board(board):
    """Construct a string representation of the board."""
    return ('\n{}\n'.format('-' * (len(board[0]) * 2 - 1))).join('|'.join(c for c in r) for r in board)


def extract_board_elements(board):
    for y, x in itertools.product(range(len(board)), range(len(board[0]))):
        piece = board[y][x]
        if piece != ' ':
            yield (piece, Point(x, y))


def make_solution_board(board_size, solution):
    solution_board = [[' '] * board_size.width for i in range(board_size.height)]
    for piece_name, point in solution:
        solution_board[point.y][point.x] = piece_name

    return solution_board


def solution_to_string(solution):
    return ';'.join('{},{},{}'.format(i[0], i[1].x, i[1].y) for i in solution)


def string_to_solution(str_solution):
    return [(j[0], Point(int(j[1]), int(j[2]))) for j in (i.split(',') for i in str_solution.split(';'))]


class ChessProblemSolver(object):
    """
    Calculate all unique configurations of a set of normal chess pieces on a chess
    board with dimensions M×N where none of the pieces is in a position to take
    any of the others.

    Arguments:
    - board_size -- an object containing 2 integer properties named width and height
      specifying the size of the board
    - piece_counts -- a dict containing a mapping of the piece identifier to its
      count. eg. {'K': 2, 'Q': 1}
    """

    def __init__(self, board_size, piece_counts, pieces_by_priority=('Q', 'K', 'R', 'N', 'B')):
        self.board_size = board_size
        self.piece_counts = piece_counts
        self.pieces_by_priority = pieces_by_priority

    def solve(self, add_rotated=True, disable_optimizations=False):
        fp, fname = mkstemp()
        try:
            self.db = sqlite3.connect(fname)
            self.db.execute('create table solutions (solution text primary key)')
            self.db.commit()

            yield from self._solve(add_rotated, disable_optimizations)
        finally:
            unlink(fname)

    def _solve(self, add_rotated=True, disable_optimizations=False):
        start_time = time()
        log.info('solving problem for %sx%s board with pieces %s',
                 self.board_size.width, self.board_size.height, self.piece_counts)
        self.steps_executed = 0

        self.board = Board(self.board_size)
        self.available_positions_head = self.board.linked_positions_head
        self.pieces = list(itertools.chain.from_iterable([p] * c for p, c in self.piece_counts.items()))
        self.pieces.sort(key=lambda item: self.pieces_by_priority.index(item), reverse=True)
        self.positioned_pieces = []
        self.already_covered = defaultdict(list)
        self.disable_optimizations = disable_optimizations

        first_piece = self.pieces.pop()

        is_square_board = self.board_size.width == self.board_size.height and not disable_optimizations
        self.is_square_board = is_square_board
        self.is_odd_square_board = is_square_board and self.board_size.width % 2 != 0

        limit_x = self.board_size.width
        limit_y = self.board_size.height
        if is_square_board:
            limit_y = int(ceil(self.board_size.height / 2))
            limit_x = int(ceil(self.board_size.width / 2))

        self.already_covered[first_piece] = [set()]

        for y in range(limit_y):
            for x in range(limit_x):
                point = Point(x, y)
                if not self.disable_optimizations and point in self.already_covered[first_piece][0]:
                    continue

                log.debug('trying %s at (%s, %s)', first_piece, x, y)
                yield from self._process_first_piece(point, first_piece)

                self.already_covered[first_piece][0].add(point.flipped(self.board_size, DIAGONAL_LEFT))
                log.debug('done trying %s', first_piece)

        total_unique_solutions = self.db.execute('select count(1) from solutions').fetchone()[0]
        log.warning('found %s unique solutions, took %s seconds and %s steps',
                    total_unique_solutions, time() - start_time, self.steps_executed)

        start_time = time()

        rotated_flipped_count = 0

        if is_square_board and add_rotated:
            self.db.commit()

            cursor = self.db.cursor()
            insert_cursor = self.db.cursor()

            cursor.execute('select solution from solutions')
            for solution in cursor:
                new_solution = sorted([(n, p.flipped(self.board_size, DIAGONAL_LEFT)) for n, p in string_to_solution(solution[0])])

                try:
                    insert_cursor.execute('insert into solutions values (?)', [solution_to_string(new_solution)])
                    log.info(
                        'added flipped solution: \n%s',
                        LogWrapper(lambda: draw_board(make_solution_board(self.board_size, new_solution))))

                    yield new_solution

                    rotated_flipped_count += 1
                except sqlite3.IntegrityError:
                    pass

            self.db.commit()

            cursor.execute('select solution from solutions')
            for solution in cursor:
                solution_board = make_solution_board(self.board_size, string_to_solution(solution[0]))

                for _ in range(3):
                    solution_board = list(zip(*solution_board[::-1]))
                    new_solution = sorted(extract_board_elements(solution_board))

                    try:
                        insert_cursor.execute('insert into solutions values (?)', [solution_to_string(new_solution)])
                        log.info(
                            'added rotated solution: \n%s',
                            LogWrapper(lambda: draw_board(solution_board)))

                        yield new_solution

                        rotated_flipped_count += 1
                    except sqlite3.IntegrityError:
                        pass

        log.warning('added %s rotated/flipped solutions and took %s seconds', rotated_flipped_count, time() - start_time)

    def _process_first_piece(self, position, piece):
        taken_grid = self.board.taken_grid
        node_grid = self.board.node_grid
        available_positions_head = self.board.linked_positions_head
        my_covered_index = len(self.already_covered[piece]) - 1

        self.partial_solution = [(piece, position)]
        removed_nodes = []
        taken_grid[position.y][position.x] = OCCUPIED
        self.already_covered[piece][my_covered_index].add(position)
        node = node_grid[position.y][position.x]
        node.remove_myself()
        removed_nodes.append(node)

        if node is available_positions_head:
            available_positions_head = node.next_node

        spread_points = list(PIECE_SPREAD[piece](self.board_size, position))
        log.debug('%s spread to points %s', piece, spread_points)
        for spread_point in spread_points:
            taken_grid[spread_point.y][spread_point.x] = THREATENED
            node = node_grid[spread_point.y][spread_point.x]
            node.remove_myself()
            removed_nodes.append(node)

            if node is available_positions_head:
                available_positions_head = node.next_node

            self.steps_executed += 1

        log.debug('board look like this now: \n%s', LogWrapper(self._visualize_grid))
        if available_positions_head is not None:
            log.debug('available positions: %s', LogWrapper(i.value for i in available_positions_head.traverse_forward()))

        for next_piece in reversed(self.pieces):
            if next_piece != piece:
                self.already_covered[next_piece] = [set()]
                log.debug('adding a set of occupied positions for next piece type:\n%s:%s', next_piece, self.already_covered[next_piece])
                break

        self.already_covered_flips = set()
        flips = []
        if not self.disable_optimizations:
            if self.is_square_board and position.x == position.y:
                flips.append(DIAGONAL_LEFT)

            if self.is_odd_square_board:
                if position.x == int(self.board_size.width / 2):
                    flips.append(HORIZONTAL)

                if position.y == int(self.board_size.height / 2):
                    flips.append(VERTICAL)

        yield from self._process_piece(available_positions_head, flips=flips)

        while removed_nodes:
            node = removed_nodes.pop()
            node.reinsert()

            self.steps_executed += 1

        for spread_point in spread_points:
            taken_grid[spread_point.y][spread_point.x] = FREE

            self.steps_executed += 1

        taken_grid[position.y][position.x] = FREE

        self.steps_executed += 1

    def _process_piece(self, original_available_positions_head, flips=()):
        try:
            piece = self.pieces.pop()
        except IndexError:
            log.debug('no more pieces to process')
            raise StopIteration

        already_covered = self.already_covered
        taken_grid = self.board.taken_grid

        already_covered[piece].append(set())
        log.debug('appending a set of already covered for myself:\n%s', already_covered[piece])
        my_covered_index = len(already_covered[piece]) - 1

        next_piece = None
        for npiece in reversed(self.pieces):
            if npiece != piece:
                next_piece = npiece
                break

        current_node = original_available_positions_head

        while current_node is not None:
            position = current_node.value
            log.debug('trying %s at %s', piece, position)

            if flips and position in self.already_covered_flips:
                current_node = current_node.next_node
                continue

            for direction in flips:
                self.already_covered_flips.add(position.flipped(self.board_size, direction))

            if flips and VERTICAL in flips and HORIZONTAL in flips:
                self.already_covered_flips.add(
                    position.flipped(self.board_size, VERTICAL).flipped(self.board_size, HORIZONTAL))

            if next_piece is not None:
                already_covered[next_piece].append(set())
                log.debug('adding a set of occupied positions for next piece type:\n%s:%s', next_piece, self.already_covered[next_piece])

            removed_nodes = []
            my_spread_points = []
            self.partial_solution.append((piece, position))

            yield from self._process_piece_inner(
                piece, my_covered_index, current_node, original_available_positions_head,
                removed_nodes, my_spread_points)

            while removed_nodes:
                node = removed_nodes.pop()
                node.reinsert()

                self.steps_executed += 1

            while my_spread_points:
                spread_point = my_spread_points.pop()
                taken_grid[spread_point.y][spread_point.x] = FREE

                self.steps_executed += 1

            taken_grid[position.y][position.x] = FREE

            if next_piece is not None:
                already_covered[next_piece].pop()

            self.partial_solution.pop()

            current_node = current_node.next_node
            self.steps_executed += 1

        self.pieces.append(piece)
        already_covered[piece].pop()

        self.steps_executed += 1

    def _process_piece_inner(self, piece, my_covered_index, current_node, available_positions_head, removed_nodes, my_spread_points):
        taken_grid = self.board.taken_grid
        node_grid = self.board.node_grid
        position = current_node.value

        already_covered = self.already_covered[piece]

        if not self.disable_optimizations:
            for covered in reversed(already_covered):
                if position in covered:
                    log.debug('position already covered by my ancestor, return')
                    raise StopIteration

        taken_grid[position.y][position.x] = OCCUPIED
        already_covered[my_covered_index].add(position)
        log.debug('i already covered a position: %s', position)
        current_node.remove_myself()
        removed_nodes.append(current_node)

        if current_node is available_positions_head:
            available_positions_head = current_node.next_node

        spread_points = list(PIECE_SPREAD[piece](self.board_size, position))
        log.debug('%s will spread to points %s', piece, spread_points)

        for spread_point in spread_points:
            if taken_grid[spread_point.y][spread_point.x] != FREE:
                self.steps_executed += 1
                if taken_grid[spread_point.y][spread_point.x] == OCCUPIED:
                    log.debug('hit a occupied position at %s, nothing more to do, go back', spread_point)
                    already_covered[my_covered_index - 1].add(position)
                    log.debug('hinting to my ancestor an always invalid position:\n%s:%s', position, already_covered)
                    raise StopIteration

                log.debug('%s is not free', spread_point)
                continue

            my_spread_points.append(spread_point)
            taken_grid[spread_point.y][spread_point.x] = THREATENED
            node = node_grid[spread_point.y][spread_point.x]
            node.remove_myself()
            removed_nodes.append(node)

            if node is available_positions_head:
                available_positions_head = node.next_node

            self.steps_executed += 1

        log.debug('%s successfully spread to points %s', piece, my_spread_points)
        log.debug('board look like this now: \n%s', LogWrapper(self._visualize_grid))
        if available_positions_head is not None:
            log.debug(
                'available positions: %s',
                LogWrapper(i.value for i in available_positions_head.traverse_forward()))

        if not self.pieces:
            new_solution = sorted(self.partial_solution[:])

            try:
                self.db.execute('insert into solutions values (?)', [solution_to_string(new_solution)])

                log.info(
                    'no more pieces, solution found! \n%s',
                    LogWrapper(lambda: draw_board(make_solution_board(self.board_size, new_solution))))

                log.info('steps executed so far: %s', self.steps_executed)

                yield new_solution
            except sqlite3.IntegrityError:
                pass

        if available_positions_head is not None:
            log.debug('process next piece')
            yield from self._process_piece(available_positions_head)

        log.debug('no more available positions, go back')

    def _visualize_grid(self):
        res = []
        for y in range(self.board_size.height):
            row = []
            for x in range(self.board_size.width):
                row.append({FREE: '-', OCCUPIED: 'O', THREATENED: 'X'}[self.board.taken_grid[y][x]])
            res.append(row)
        return draw_board(res)


def cli_main():
    """The CLI entry point"""

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('board_size', help='Eg. 10x10')
    parser.add_argument('piece_count', nargs='+', help='Eg. K:3. Available pieces: (K)ing, (Q)ueen, (R)ook, (B)ishop, k(N)ight')
    parser.add_argument('-v', '--verbose', action='count', help='verbose output. -v = INFO, -vv = DEBUG')
    parser.add_argument('-o', metavar='FILE_PATH', help='save results to output file')
    parser.add_argument('-R', action='store_true', help='dont add rotated variants to solutions')
    parser.add_argument('-S', action='store_true', help='disable optimizations')

    args = parser.parse_args()

    level = {
        None: logging.WARNING,
        1: logging.INFO,
        2: logging.DEBUG,
    }.get(args.verbose, logging.DEBUG)

    logging.basicConfig(level=level)

    solver = ChessProblemSolver(
        BoardSize(*[int(i) for i in args.board_size.split('x')]),
        {k: int(v) for k, v in (i.split(':') for i in args.piece_count)}
    )

    solutions = solver.solve(add_rotated=not args.R, disable_optimizations=args.S)

    if args.o is not None:
        fp = open(args.o, 'w')

    for solution in solutions:
        if args.o is not None:
            fp.write('= {}\n'.format(solution))
        # fp.write(draw_board(make_solution_board(solver.board_size, solution)))
        # fp.write('\n\n')

    if args.o is not None:
        fp.close()


if __name__ == '__main__':
    cli_main()
