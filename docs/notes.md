- think of a mean to represent piece movements
- find a data structure to represent the board
- based on the algorithm design manual, it seems to be a combinatorial problem
- put a piece on the board and mark all possible paths. each additional piece
  can only be placed on non marked positions
- on a square board an easy optimization seems to be that the first piece only
  needs to cover 1/4 of the board, then all results can be rotated 90º 3 times
- it seems advantageous to try the pieces that cover the largest area first
- the naive approach won't cut it, it would be a quadratic algorithm that 
  wouldn't give the asked solution in a reasonable time
- need a way to remove marked slots on the board so I don't iterate on then to
  find the next free slot for the next piece

![](visual-example1.png)

![](visual-example2.png)

![](visual-example3.png)


# outline of possible solution 1

- define the pieces with a function with their spreading algorithm. maybe defining
  then as running clockwise starting from 180º will be more effective at
  finding collisions early.
- the board will have two parts, a doubly linked list connecting the slots
  sequentially and a 2 dimensional array indexing the nodes from the list. the node
  will have to know its coordinates
- get the list of pieces sorted by their maximum possible area (this will be a constant)
- for each position on the board:

    - PROCESS_FIRST_PIECE
    - the first piece is important because it doesn't need to traverse the entire board,
      start from the 0,0 coordinate and go up to half the width and height of the board.
    - mark its x,y coordinate as taken and remove the indexed node from the linked list,
      append to a removed nodes stack and save the preceding node
    - run the spreading algorithm, for each taken position, remove the indexed node,
      save the preceding node and append it to a removed nodes stack
    - PROCESS_PIECE

        - remove a piece from the list and put it in a second list
        - start traversing the linked list:
          - get the next node to process from the current node, if its None start with the
            list head and save it as the current
          - mark the x,y coordinates retrieved from the node as taken
          - remove the node from the linked list, append it to the stack of removed nodes,
            saving its preceding node, removed count = 1
          - run the spreading algorithm, for each taken position:

              - if the node is in the list, remove it, increment the removed count
                and append it to the removed nodes stack saving its preceding node

              - if the spreading doesn't hit any taken x,y coordinate and the list of
                pieces has 1 element, a solution was found, undo previous steps and continue

              - if the spreading hits a taken coordinate or there are no more items in the
                linked list, stop, undo previous steps and continue

              - else PROCESS_PIECE

          - UNDO_STEPS

              - while the removed count > 0:

                  - pop a node from the stack of removed and readd it to the linked list
                  - unmark its x, y coordinates as taken
                  - decrement the removed count

## potential optimizations

- it seems like the same square board optimization can be applied with a tweak on
  non-square boards
- it seems like a position previously covered by a piece of same type can be ignored
- it might be advantageous to, when the spreading area is bigger than the number
  of pieces already positioned, to directly check positioned pieces for collisions 
- find a way to create a blacklist to prevent the evaluation of a position which is
  known to have a collision

solve:
R:[{}]

process first

R:[{(0, 0)}]
K:[{}]

process piece > R

R:[{(0, 0)}, {(1, 1)}]
K:[{}, {}]

process piece > R > K

R:[{(0, 0)}, {(1, 1)}]
K:[{(2, 2)}, {(2, 2)}]


think:

    lets say there are two rooks positioned like:

    R - - -
    - - - -
    - - - -
    - - R -   partial solution: R,0,0 -> R,2,3

    R:[[(0, 0)], []]
    K:[[], []]

    now I want to position a K, at the first available place:

    R - - -
    - K - -
    - - - -
    - - R -

    K[-1].add(K position)

    that position is invalid, since K would take R. now the next:

    R - - -
    - - - K
    - - - -
    - - R -   partial solution: R,0,0 -> R,2,3 -> K,3,1

    now after K trying all positions, its time to move his parent to the next:

    R - - -
    - - - -
    - - - -
    - - - R   partial solution: R,0,0 -> R,3,3 -> K?

    now its time to position K again. how to detect that the first available place is
    a dead end? need a way to preserve the knowlegde of the partial past. seems like
    i need to ignore my previous parent and check collisions with hist previous level


    now lets say I'll position another K at this board:

    R - - -
    - - - K
    - - - -
    - - R -   partial solution: R,0,0 -> R,2,3 -> K,3,1

    K will already ignore a position traveled by a previous K, so it will try:

    R - - -
    - - - K
    - K - -
    - - R -

    that position is invalid, ok. now how do i warn my parent K that that position
    is invalid?